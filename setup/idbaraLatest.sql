-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: idibara
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arcSurat`
--

DROP TABLE IF EXISTS `arcSurat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arcSurat` (
  `id_surat` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `tipeSurat` enum('SIP Lokal','SIP Tamu','Mutasi') DEFAULT 'SIP Lokal',
  `nomorSurat` varchar(30) NOT NULL,
  `tanggalSurat` date DEFAULT NULL,
  `id_dokter` int(8) NOT NULL,
  `sip_ke` int(1) DEFAULT '1',
  `tempatpraktek1` tinytext,
  `alamatpraktek1` tinytext,
  `jadwalpraktek1` tinytext,
  `tempatpraktek2` tinytext,
  `alamatpraktek2` tinytext,
  `jadwalpraktek2` tinytext,
  `tempatpraktek3` tinytext,
  `alamatpraktek3` tinytext,
  `jadwalpraktek3` tinytext,
  `pekerjaan` varchar(30) DEFAULT NULL,
  `nmrekomendasi` varchar(30) DEFAULT NULL,
  `idi_tujuan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_surat`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arcSurat`
--

LOCK TABLES `arcSurat` WRITE;
/*!40000 ALTER TABLE `arcSurat` DISABLE KEYS */;
INSERT INTO `arcSurat` VALUES (00000001,'Mutasi','1124/CAB.BNA/A.3/07/2019','2019-07-30',2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Manado'),(00000002,'SIP Lokal','/CAB.BNA/A.3/08/2019','2019-08-01',1,1,'asasa','aasas','123','adad','lhl','ojoo','ojo','o','jojo',NULL,NULL,NULL),(00000003,'SIP Lokal','/CAB.BNA/A.3/08/2019','2019-08-01',1,1,'asasa','aasas','123','adad','lhl','ojoo','ojo','o','jojo',NULL,NULL,NULL),(00000004,'Mutasi','143/CAB.BNA/A.3/08/2019','2019-08-02',3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Kertosono'),(00000005,'Mutasi','123/CAB.BNA/A.3/08/2019','2019-08-02',3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Tangerang Selatan'),(00000006,'Mutasi','123/CAB.BNA/A.3/08/2019','2019-08-02',3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Tangerang Selatan');
/*!40000 ALTER TABLE `arcSurat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dokter`
--

DROP TABLE IF EXISTS `dokter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dokter` (
  `id_dokter` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  `tempat_lahir` varchar(20) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jk` enum('Pria','Wanita') DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `instansi` varchar(50) DEFAULT NULL,
  `hp` varchar(12) DEFAULT NULL,
  `pt1` tinytext,
  `no_ijazah1` tinytext,
  `tahun_lulus` int(4) DEFAULT NULL,
  `kompetensi1` tinytext,
  `pt2` tinytext,
  `no_ijazah2` tinytext,
  `kompetensi2` tinytext,
  `tahun_lulus2` int(4) DEFAULT NULL,
  `no_kta` int(6) unsigned DEFAULT NULL,
  `berlaku_kta` date DEFAULT NULL,
  `no_str` tinytext,
  `berlaku_str` date DEFAULT NULL,
  `status_idi` varchar(20) DEFAULT NULL,
  `idi_asal` varchar(20) DEFAULT NULL,
  `norek_idi` varchar(20) DEFAULT NULL,
  `tahun_masuk` int(4) DEFAULT NULL,
  `foto` tinytext,
  `keterangan` tinytext,
  PRIMARY KEY (`id_dokter`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dokter`
--

LOCK TABLES `dokter` WRITE;
/*!40000 ALTER TABLE `dokter` DISABLE KEYS */;
INSERT INTO `dokter` VALUES (00000001,'Paidi Nurdiman','Tenggarong','1971-08-22','Pria','Jl. Mengkudu No. 9 Kebon Tangga','Dinas Peternakan','0888767066','Univ. Boroksubur','00910',1994,'Dokter Binatang','Univ. Sidomuncul','00911','Spesialis Jamu',1996,100200,'2025-08-25','12134','2021-09-12','Anggota Biasa','Banjarnegara','11231',1999,'obama-call.png','Farmakolog Herbal'),(00000002,'Chyntia Wu Chi','Beijing','1980-10-05','Pria','Perum Pura Mandiri','PKU Muhammadiyah Mandiraja','0286595737','Univ. Muhammadiyah Klampok','1111',2004,'Dokter Ora Umum','','','',0,222298,'2020-08-17','3333','2020-08-15','Ndableg','Purbalingga','4444',2008,'chintya-khan.jpg','Kolektor Anak Yatim'),(00000003,'Johan Isaishi','Kyoto','1960-10-24','Pria','Perum Serayu Buthek No. 34','Dinkes Kab. Banjarnegara','0286592333','Univ Nagoya','202001',1985,'Dokter Gizi','Univ. Guang Zhou','302355','Sp. Jantung Pisang',1992,212001,'2020-05-29','212002','2023-06-17','mutasi','Cimahi','202112',2000,'joe_hisaishi.jpg','Master Pijat Piano'),(00000004,'Karena Lika','Bandung','1977-09-08','Wanita','Villa Sigaluh Noisy','Puskesmas Sigaluh 1','081388881210','Univ. Gajah Mungkur','30123',2000,'Dokter Umum','Univ. Indosato','34034','Sp. Kulit Duren',2005,312304,'2020-08-08','32313','2022-09-09','Anggota','Jombang','43231',2005,'alika.png','Manis Legit');
/*!40000 ALTER TABLE `dokter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iuran`
--

DROP TABLE IF EXISTS `iuran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iuran` (
  `id_iuran` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_dokter` int(8) unsigned zerofill NOT NULL,
  `jenis_iuran` varchar(20) DEFAULT NULL,
  `mekanisme` varchar(20) DEFAULT NULL,
  `lokasi` varchar(20) DEFAULT NULL,
  `iuran_dari` date DEFAULT NULL,
  `iuran_sampai` date DEFAULT NULL,
  `nominal` int(11) unsigned DEFAULT NULL,
  `untuk_bayar` tinytext,
  `tanggal_transaksi` date DEFAULT NULL,
  `keterangan` tinytext,
  PRIMARY KEY (`id_iuran`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iuran`
--

LOCK TABLES `iuran` WRITE;
/*!40000 ALTER TABLE `iuran` DISABLE KEYS */;
INSERT INTO `iuran` VALUES (00000001,00000001,'Iuran Bulanan','Transfer','Indomaret','2019-07-01','2019-09-30',300000,'Iuran Bulanan','2019-07-15','Tunai'),(00000002,00000003,'Iuran Bulanan','Transfer','Bank Japra','2019-07-01','2019-12-31',600000,'Iuran Bulanan','2019-07-12','Diterima'),(00000003,00000004,'Bulanan','Transfer','BCA','2019-04-01','2019-06-30',300000,'Iuran Bulanan IDI','2019-06-10','diterima');
/*!40000 ALTER TABLE `iuran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mutasi`
--

DROP TABLE IF EXISTS `mutasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mutasi` (
  `id_mutasi` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_dokter` int(8) unsigned zerofill NOT NULL,
  `idi_tujuan` varchar(20) DEFAULT NULL,
  `norek` varchar(20) DEFAULT NULL,
  `tgl_mutasi` date DEFAULT NULL,
  `keterangan` tinytext,
  PRIMARY KEY (`id_mutasi`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mutasi`
--

LOCK TABLES `mutasi` WRITE;
/*!40000 ALTER TABLE `mutasi` DISABLE KEYS */;
INSERT INTO `mutasi` VALUES (00000001,00000003,'Tangerang Selatan','123/CAB.BNA/A.3/08/2','2019-07-28',NULL);
/*!40000 ALTER TABLE `mutasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengurus`
--

DROP TABLE IF EXISTS `pengurus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengurus` (
  `id_pengurus` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `periode` varchar(9) NOT NULL,
  `id_dokter` int(8) unsigned zerofill NOT NULL,
  `jabatan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_pengurus`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengurus`
--

LOCK TABLES `pengurus` WRITE;
/*!40000 ALTER TABLE `pengurus` DISABLE KEYS */;
INSERT INTO `pengurus` VALUES (00000003,'2018/2021',00000002,'3'),(00000004,'2018/2021',00000003,'1'),(00000005,'2018/2021',00000001,'2'),(00000006,'2018/2021',00000004,'5');
/*!40000 ALTER TABLE `pengurus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `praktik`
--

DROP TABLE IF EXISTS `praktik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `praktik` (
  `id_praktik` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_dokter` int(8) unsigned zerofill DEFAULT NULL,
  `sip1` varchar(50) DEFAULT NULL,
  `nosip1` varchar(15) DEFAULT NULL,
  `sip2` varchar(50) DEFAULT NULL,
  `nosip2` varchar(15) DEFAULT NULL,
  `sip3` varchar(50) DEFAULT NULL,
  `nosip3` varchar(15) DEFAULT NULL,
  `keterangan` tinytext,
  PRIMARY KEY (`id_praktik`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `praktik`
--

LOCK TABLES `praktik` WRITE;
/*!40000 ALTER TABLE `praktik` DISABLE KEYS */;
INSERT INTO `praktik` VALUES (00000001,00000001,'Pasar Bandingan','1221','RM Sari Rahayu','121','Khitan Pak Yono','212','Terima Pesanan'),(00000003,00000003,'Simpang Singamerta','2122','Depo Pelita','2123','Gembongan','2124','Tarif Bersahabat'),(00000004,00000002,'Perum Serayu Buthek','1212','Simpang Semampir','535353','Pasar Pucang','7654','Siap panggil');
/*!40000 ALTER TABLE `praktik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surat`
--

DROP TABLE IF EXISTS `surat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surat` (
  `id_surat` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomor_surat` varchar(30) DEFAULT NULL,
  `jenis_surat` varchar(15) DEFAULT NULL,
  `isi` tinytext,
  `tujuan` varchar(20) DEFAULT NULL,
  `id_dokter` int(8) unsigned zerofill NOT NULL,
  `sip_ke` varchar(1) DEFAULT NULL,
  `alamat_sip1` tinytext,
  `alamat_sip2` tinytext,
  `alamat_sip3` tinytext,
  `keterangan` tinytext,
  PRIMARY KEY (`id_surat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surat`
--

LOCK TABLES `surat` WRITE;
/*!40000 ALTER TABLE `surat` DISABLE KEYS */;
/*!40000 ALTER TABLE `surat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-03  5:14:31
