-- CREATE DATABASE 
DROP DATABASE IF EXISTS idibara;
CREATE DATABASE idibara;

-- GRANT USER
GRANT ALL ON idibara.* TO purwanto@localhost IDENTIFIED BY 'skripsiPurw@nt0' WITH GRANT OPTION;

USE idibara;

-- TABLE: USER
DROP TABLE IF EXISTS user;
CREATE TABLE user(
id int(3) not null auto_increment,
username varchar(10) not null,
password varchar(32) not null,
primary key(id)
);

-- TABLE: PENGURUS
DROP TABLE IF EXISTS pengurus;
CREATE TABLE pengurus(
id_pengurus int(8) unsigned zerofill not null auto_increment,
periode varchar(9) not null,
id_dokter int(8) unsigned zerofill not null,
jabatan varchar(30),
primary key (id_pengurus)
);

-- TABLE: IURAN
DROP TABLE IF EXISTS iuran;
CREATE TABLE iuran(
id_iuran int(8) unsigned zerofill auto_increment,
id_dokter int(8) unsigned zerofill not null,
jenis_iuran varchar(20),
mekanisme varchar(20),
lokasi  varchar(20),
iuran_dari date,
iuran_sampai date,
nominal int(11),
untuk_bayar tinytext,
tanggal_transaksi date,
keterangan tinytext,
primary key (id_iuran)
);

--TABLE: MUTASI
DROP TABLE IF EXISTS mutasi;
CREATE TABLE mutasi(
id_mutasi int(8) unsigned zerofill auto_increment,
id_dokter int(8) unsigned zerofill not null,
idi_tujuan varchar(20),
norek varchar(20),
tgl_mutasi date,
keterangan tinytext,
primary key(id_mutasi)
);

-- TABLE: DOKTER
DROP TABLE IF EXISTS dokter;
CREATE TABLE dokter(
id_dokter int(8) unsigned zerofill auto_increment,
nama varchar(30),
tempat_lahir varchar(20),
tgl_lahir date,
jk enum('Pria','Wanita'),
alamat varchar(50),
instansi varchar(20),
np int(12),
pt1 tinytext,
no_ijazah1 tinytext,
tahun_lulus int(4),
kompetensi1 tinytext,
pt2 tinytext,
no_ijazah2 tinytext,
kompetensi2 tinytext,
tahun_lulus2 int(4),
no_kta int(6) unsigned,
berlaku_kta date,
no_str tinytext,
berlaku_str date,
status_idi varchar(20),
idi_asal varchar(20),
norek_idi varchar(20),
tahun_masuk int(4),
foto tinytext,
keterangan tinytext,
primary key(id_dokter)
);

-- TABLE: PRAKTIK
DROP TABLE IF EXISTS praktik;
CREATE TABLE praktik(
 id_praktik int(8) unsigned zerofill auto_increment,
 id_dokter int(8) unsigned zerofill,
 sip1 varchar(50),
 nosip1 varchar(15),
 sip2 varchar(50),
 nosip2 varchar(15),
 sip3 varchar(50),
 nosip3 varchar(15),
 keterangan tinytext,
 primary key(id_praktik)
);

-- TABLE: SURAT
DROP TABLE IF EXISTS surat;
CREATE TABLE surat(
 id_surat int(8) unsigned zerofill auto_increment,
 jenis_surat varchar(15),
 isi tinytext,
 tujuan varchar(20),
 id_dokter int(8) unsigned zerofill not null,
 sip_ke varchar(1),
 alamat_sip1 tinytext,
 alamat_sip2 tinytext,
 alamat_sip3 tinytext,
 keterangan tinytext,
 primary key(id_surat)
);
