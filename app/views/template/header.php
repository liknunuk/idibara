<?php
session_start();
if(!isset($_SESSION['user']) || $_SESSION['user'] == NULL ){
  header("Location: " .BASEURL."/Home/Login");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?= $data['title']; ?></title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="<?=BASEURL;?>/img/logo-idi.png" width="16px" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap 4 CDN -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= BASEURL .'/css/idibara.css'; ?>">
  <script> var pusdata = "<?=PUSDATA;?>"; var baseurl = "<?=BASEURL;?>"; </script>
</head>
<body class='idi-body'>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid"  id="idi-nav">
      <a class="navbar-brand" href="<?= BASEURL; ?>">
      <img src="<?= BASEURL; ?>/img/logo-idi.png" alt="logo idi" class="brand-img">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link active" href="<?= BASEURL; ?>">Beranda <span class="sr-only">(current)</span></a>
          <a class="nav-item nav-link" href="<?= BASEURL; ?>/dokter">Dokter</a>
          <a class="nav-item nav-link" href="<?= BASEURL; ?>/pengurus">Pengurus</a>
          <a class="nav-item nav-link" href="<?= BASEURL; ?>/praktik">Praktik</a>
          <!--a class="nav-item nav-link" href="<?= BASEURL; ?>/iuran">Iuran</a-->
          <!-- drop down iuran-->
          <div class="dropdown">
            <a href="#" class="nav-dd nav-item nav-link dropdown-toggle" id="ddIuran" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Iuran
            </a>
            <div class="dropdown-menu" aria-labelledby="ddIuran">
              <a class="dropdown-item" href="<?= BASEURL; ?>/iuran">Data Iuran</a>
              <a class="dropdown-item" href="<?= BASEURL; ?>/iuran/rekap">Rekap Iuran</a>
              <a class="dropdown-item" href="<?= BASEURL; ?>/iuran/tagihan">Tagihan</a>
            </div>
        </div>
          <!-- drop down iuran-->
          
          <!-- drop down surat-->
          <div class="dropdown">
            <a href="#" class="nav-dd nav-item nav-link dropdown-toggle" id="ddsurat" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Rekomendasi
            </a>
            <div class="dropdown-menu" aria-labelledby="ddsurat">
              <a class="dropdown-item" href="<?= BASEURL; ?>/surat/siplokal">SIP REGULER</a>
              <a class="dropdown-item" href="<?= BASEURL; ?>/surat/siptamu">SIP TAMU</a>
              <a class="dropdown-item" href="<?= BASEURL; ?>/surat/mutasi">MUTASI</a>
              <a class="dropdown-item" href="<?= BASEURL; ?>/surat/arsip">ARSIP</a>
            </div>
        </div>
          <!-- drop down surat-->

          <a class="nav-item nav-link" href="<?= BASEURL; ?>/mutasi">Mutasi</a>
        </div>
      </div>
          <a class="nav-item nav-link" href="<?= BASEURL; ?>/Home/logout">
          <button class="btn btn-default bg-light text-dark">Logout</button>
          </a>
      <div>

      </div>
    </div>
  </nav>
