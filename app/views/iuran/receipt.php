<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nota Pembayaran</title>
    <style>
        p,h1,h2,h3,h4,h5,h6{margin:0; padding:0;}
        #logo-idi { width: 120px; margin: 10px;}
        .fz-10 { font-size: 10pt; text-align: center; }
        .ca { text-align: center; }
    </style>
</head>
<body onload=window.print();>
    <table width="100%">
        <tbody>
            <tr>
                <td width="140px">
                    <img src="<?=BASEURL;?>/img/logo-idi.png" id="logo-idi" alt="idi banjarnegara">
                </td>
                <td align="center">
<h4>IKATAN DOKTER INDONESIA</h4>
<h5>(THE INDONESIAN MEDICAL ASSOCIATION)<br>
IDI CABANG BANJARNEGARA</h5>
<p class="fz-10">Alamat Sekretariat : Kantor Dinas Kesehatan Kabupaten Banjarnegara<br>
Jl. Selamanik No.8, Kelurahan Semampir – Banjarnegara Kode Pos 53418<br>
Telp. / WA : (0286) 591080 / 0853 2509 1551 @mail : idi_bna@yahoo.com / Website : www.idibanjarnegara.com</p>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="ca">
        <h3>TANDA TERIMA PEMBAYARAN</h3>
    </div>
    <table width="640" align="center" border="0" cellspacing="0">
        <tbody>
            <tr>
                <td width="200">Nomor Receipt</td>
                <td>:&nbsp;<?=$data['iuran']['id_iuran'];?></td>
            </tr>
            <tr><td>Telah terima dari</td><td>:&nbsp;<?=$data['iuran']['nama'];?></td></tr>
            <tr><td>Tanggal</td><td>:&nbsp;<?=tanggal($data['iuran']['tanggal_transaksi']);?></td></tr>
            <tr><td>Uang sebesar</td>
                <td>:&nbsp;
                <?=number_format($data['iuran']['nominal'],2,',','.');?>
                (<i><?=terbilang($data['iuran']['nominal']);?> Rupiah</i>)
                </td>
            </tr>
            <tr>
                <td>Untuk Membayar</td>
                <td>
                :&nbsp;<?=$data['iuran']['untuk_bayar'];?>, &nbsp;
                <?php echo tanggal($data['iuran']['iuran_dari']). " - " . tanggal($data['iuran']['iuran_sampai']); ?>
                </td>
            </tr>
            <tr><td>Mekanisme/Tempat</td><td>:&nbsp;<?=$data['iuran']['mekanisme'].'-'.$data['iuran']['lokasi'];?></td></tr>
            <tr><td></td><td></td></tr>
            <tr>
                <td></td>
                <td align="center">
                    Banjarnegara, <?=date('d / m / Y');?><br/><br/><br/>
                    
                    <?=$data['bandar']['nama'];?><br/>
                    NPA IDI: <?=$data['bandar']['no_kta'];?><br/> 
                </td>
            </tr>
        </tbody>
    </table>
    <!--?php print_r($data);?-->
</body>
</html>

<?php
function terbilang($nilai) {
    if($nilai<0) {
        $hasil = "minus ". trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}

function penyebut($nilai) {
$nilai = abs($nilai);
$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
$temp = "";
if ($nilai < 12) {
  $temp = " ". $huruf[$nilai];
} else if ($nilai < 20) {
  $temp = penyebut($nilai - 10). " belas";
} else if ($nilai < 100) {
  $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
} else if ($nilai < 200) {
  $temp = " seratus" . penyebut($nilai - 100);
} else if ($nilai < 1000) {
  $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
} else if ($nilai < 2000) {
  $temp = " seribu" . penyebut($nilai - 1000);
} else if ($nilai < 1000000) {
  $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
} else if ($nilai < 1000000000) {
  $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
} else if ($nilai < 1000000000000) {
  $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
} else if ($nilai < 1000000000000000) {
  $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
}
return $temp;
}

function tanggal($tgl){
    $namaBulan = array("01"=>"Januari","02"=>"Februari","03"=>"Maret","04"=>"April","05"=>"Mei","06"=>"Juni","07"=>"Juli","08"=>"Agustus","09"=>"September","10"=>"Oktober","11"=>"Nopember","12"=>"Desember");
    list($t,$b,$h) = explode("-",$tgl);
    return $h . " " . $namaBulan[$b] . " " . $t;
}
?>