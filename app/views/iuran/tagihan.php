<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card px-5 mt-3">
                <div class="card-header">
                    <div class="col-md-12">
                        <h1 class="pageTitle">TAGIHAN HINGGA BULAN <?=date("m-Y");?></h1>
                    </div>  
                </div>
                <div class="card-body col-5 mx-auto">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="bg-primary">
                                <th>Nomor</th>
                                <th>Dokter</th>
                                <th>Iuran Terakhir</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $nomor=1; foreach($data['tagihan'] AS $tagihan): ?>
                        <tr>
                            <td><?=$nomor;?></td>
                            <td><?=$tagihan['nama'];?></td>
                            <td><?=$this->ymd2dmy($tagihan['iuran_sampai']);?></td>
                        </tr>
                        <?php $nomor+=1; endforeach; ?>
                        </tbody>
                        <tr>
                            <td colspan='3' class='print-hide text-center'>
                                <button class="btn btn-primary" onClick=window.print()>cetak</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>