<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card px-5 mt-3">
                <div class="card-header">
                    <div class="col-md-12">
                        <h1 class="pageTitle">REKAP IURAN TAHUN <?=date("Y");?></h1>
                    </div>  
                </div>
                <div class="card-body col-8 mx-auto">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="bg-primary">
                                <th>Nomor</th>
                                <th>Bulan</th>
                                <th>Jumlah Iuran Masuk</th>
                                <th width='50' class="print-hide">Cetak</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $totIuran = 0 ; ?>
                            <?php $nomor=1; foreach($data['rekap'] AS $rekap): ?>
                            <tr>
                                <td><?=sprintf("%02d",$nomor);?></td>
                                <td><?=$this->ym2my($rekap['bulan']);?></td>
                                <td align="right"><?=$this->nominal($rekap['jumlah']);?></td>
                                <td class="print-hide text-center">
                                    <button class="btn btn-primary"
                                    onclick = window.open("<?=BASEURL.'/Laporan/iuran/'.$rekap['bulan'];?>")>
                                        <i class="fa fa-print"></i>
                                    </button>
                                </td>
                            </tr>
                            <?php $totIuran+=$rekap['jumlah']; ?>
                            <?php $nomor++; endforeach; ?>
                            <tr class="bg-warning text-dark">
                                <td colspan="2">Jumlah</td>
                                <td align="right"><?=$this->nominal($totIuran);?></td>
                                <td class="print-hide">&nbsp;</td>
                            </tr>
                            <tr class="print-hide text-center">
                                <td colspan='4'>
                                <button class="btn btn-primary" onClick=window.print()>Cetak</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>