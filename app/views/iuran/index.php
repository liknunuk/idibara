<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <h1 class="pageTitle">DATA IURAN ANGGOTA</h1>
    </div>  
  </div>
  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
        <div>
          <nav class="navbar navbar-light bg-light">
            <div>
            <i class="fa fa-plus-square btn btn-success mb-1 mt-2 py-3" style='font-size: 18px;' data-toggle="modal" data-target="#modalIuran"> Iuran Baru</i>
            <i class="fa fa-print btn btn-primary mb-1 mt-2 py-3" style='font-size: 18px;' onclick=window.open("<?=BASEURL;?>/laporan/iuran")></i>
            </div>
            <form class="form-inline" id="iur_fsrc">
              <input class="form-control mr-sm-2" type="date" placeholder="Search" aria-label="Search" id="iur_fdate">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit" id="iur_sbmt">Search</button>
            </form>
          </nav>
        </div>
        <ul class="list-inline print-hide text-center">
          <li class="list-inline-item">Pilih Bulan</li>
          <?php
          for($bl = 1 ; $bl < 13 ; $bl++ ){
            echo "<li class='list-inline-item'> <button class='btn btn-info'
            onclick=window.location='".BASEURL."/iuran/".date('Y')."/".sprintf("%02d",$bl)."'
            >".sprintf("%02d",$bl)."</button></li>";
          }
          ?>
        </ul>

        <table class="table table-striped table-sm">
          <thead>
            <tr class="bg-info">
              <th>ID Iuran</th>
              <th>Tanggal</th>
              <th>Nama Doker</th>
              <th width="150">Jumlah</th>
              <th>Keterangan</th>
              <th width="225" class="print-hide">Kontrol</th>
            </tr>
          </thead>
          <tbody id="iur_data">
            <?php foreach($data['iuran'] as $iuran ): ?>
            <tr>
              <td><?=$iuran['id_iuran'];?></td>
              <td><?=$this->ymd2dmy($iuran['tanggal_transaksi']);?></td>
              <td><?=$iuran['nama'];?></td>
              <td align="right"><?=$this->nominal($iuran['nominal']);?>&nbsp;</td>
              <td><?=$iuran['untuk_bayar'];?></td>
              <td class="print-hide">
                <i class="fa fa-credit-card btn btn-primary iur-info"></i>
                <i class="fa fa-edit btn btn-warning iur-edit"></i>
                <i class="fa fa-trash-o btn btn-danger iur-rmve"></i>
                <i class="fa fa-print btn btn-success iur-rcpt"></i>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
          
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modalIuran">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h5 class="modal-title text-light">Form Iuran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=BASEURL ;?>/iuran/save" method="post" class="mt-3">
          <input type="hidden" name="iur_mod" id="iur_mod" value="baru">

          <div class="form-group row">
            <label for="iur_id" class="col-sm-4">ID Iuran</label>
            <div class="col-sm-8">
              <input type="text" name="iur_id" id="iur_id" class="form-control" readonly>
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_dokter" class="col-sm-4">ID Dokter</label>
            <div class="col-sm-8">
              <input type="text" name="iur_dokter" id="iur_dokter" class="form-control" list="dokList" placeholder="Tulis nama dokter">
              <datalist id="dokList"></datalist>
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_jenis" class="col-sm-4">Jenis Iuran</label>
            <div class="col-sm-8">
              <input type="text" name="iur_jenis" id="iur_jenis" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="iur_mekanisme" class="col-sm-4">Mekanisme Iuran</label>
            <div class="col-sm-8">
              <input type="text" name="iur_mekanisme" id="iur_mekanisme" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_lokasi" class="col-sm-4">Lokasi Iuran</label>
            <div class="col-sm-8">
              <input type="text" name="iur_lokasi" id="iur_lokasi" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_mulai" class="col-sm-4">Mulai Tanggal</label>
            <div class="col-sm-8">
              <input type="date" name="iur_mulai" id="iur_mulai" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_sampai" class="col-sm-4">Sampai Tanggal</label>
            <div class="col-sm-8">
              <input type="date" name="iur_sampai" id="iur_sampai" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_nominal" class="col-sm-4">Jumlah Nominal</label>
            <div class="col-sm-8">
              <input type="number" name="iur_nominal" id="iur_nominal" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_untuk" class="col-sm-4">Untuk Pembayaran</label>
            <div class="col-sm-8">
              <input type="text" name="iur_untuk" id="iur_untuk" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_tanggal" class="col-sm-4">Tanggal Pembayaran</label>
            <div class="col-sm-8">
              <input type="date" name="iur_tanggal" id="iur_tanggal" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <label for="iur_note" class="col-sm-4">Keterangan</label>
            <div class="col-sm-8">
              <input type="text" name="iur_note" id="iur_note" class="form-control">
            </div>
          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modalIurinfo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h5 class="modal-title text-light">Detail Iuran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-sm">
            <tbody>
              <tr>
                <th>ID Iuran</th><td id="dinf_idx"></td>
              </tr>
              <tr>
                <th>Nama Dokter</th><td id="dinf_dok"></td>
              </tr>
              <tr>
                <th>Tanggal</th><td id="dinf_tgl"></td>
              </tr>
              <tr>
                <th>Jumlah</th><td align="right" id="dinf_nominal"></td>
              </tr>
              <tr>
                <th>Untuk Pembayaran</th><td id="dinf_untuk"></td>
              </tr>
              <tr>
                <th>Periode Iuran</th><td id="dinf_periode"></td>
              </tr>
              <tr>
                <th>Jenis Iuran</th><td id="dinf_jenis"></td>
              </tr>
              <tr>
                <th>Mekanisme</th><td id="dinf_mekanisme"></td>
              </tr>
              <tr>
                <th>Lokasi</th><td id="dinf_lokasi"></td>
              </tr>
              <tr>
                <th>Keterangan</th><td id="dinf_keterangan"></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- id_iuran, id_dokter, jenis_iuran, mekanisme, lokasi, iuran_dari , iuran_sampai , nominal , untuk_bayar , tanggal_transaksi , keterangan-->
<script> var baseurl = "<?=BASEURL;?>";</script>
<script src="<?=BASEURL;?>/js/iurIndex.js"></script>