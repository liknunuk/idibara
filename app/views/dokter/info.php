<div class="container-fluid">
    <div class="row mt-2 px-3">
        <div class="col-12">
        <h1 class="pageTitle">PROFIL DOKTER</h1>
        </div>
        <div class="table-responsive" style="padding:10px 50px;">
            <?php 
                list($th,$bl,$tg) = explode('-',$data['dokter']['tgl_lahir']);
                $tglahir = $tg . ' / ' . $bl . ' / ' . $th;
                list($th1,$bl1,$tg1) = explode('-',$data['dokter']['berlaku_kta']);
                list($th2,$bl2,$tg2) = explode('-',$data['dokter']['berlaku_str']);
                
            ?>
            <table class="table table-bordered table-sm">
                <tbody class='bg-light'>
                    <tr>
                        <td rowspan="13" width="250px">
                        <?php if($data['dokter']['foto'] == '' ): ?>
                            <img src="<?=BASEURL;?>/img/anon-doctor.jpg" alt="Dokter" width='180px' height="240px" style="margin-left: 35px;">
                        <?php else: ?>
                        <img src="<?=BASEURL;?>/foto/<?=$data['dokter']['foto'];?>" alt="Dokter" width='180px' height="240px" style="margin-left: 35px;">
                        <?php endif; ?>
                        <div style="text-align:center;" class="mt-2"><button class="btn btn-success print-hide" id="gantiFoto">Ganti Foto</button></div>
                        </td>
                        <th width='300'>ID Dokter</th><td id="lblDokId"><?=$data['dokter']['id_dokter'];?></td></tr>
                    <tr><th>Nama Lengkap / Jenis Kelamin</th><td><?=$data['dokter']['nama'] . ', ' . $data['dokter']['jk'];?></td></tr>
                    <tr><th>Tempat dan Tanggal Lahir</th><td><?=$data['dokter']['tempat_lahir'] . ', ' . $tglahir;?></td></tr>
                    <tr><th>Alamat & No. telp</th><td><?=$data['dokter']['alamat'] . ' - ' . $data['dokter']['hp'];?></td></tr>
                    <tr><th>Instansi</th><td><?=$data['dokter']['instansi'] ;?></td></tr>
                    <tr>
                        <th>Pendidikan Kedokteran</th>
                        <td>
                        <?=$data['dokter']['pt1'] ;?> Lulus Tahun <?=$data['dokter']['tahun_lulus'] ;?> No. Ijazah : <?=$data['dokter']['no_ijazah1'] ;?>. Kompetensi <?=$data['dokter']['kompetensi1'] ;?>
                        </td>
                    </tr>
                    <tr>
                        <th>Pendidikan Spesialis</th>
                        <td>
                        <?php if($data['dokter']['pt2'] !== '' ): ?>
                        <?=$data['dokter']['pt2'] ;?> Lulus Tahun <?=$data['dokter']['tahun_lulus2'] ;?> No. Ijazah : <?=$data['dokter']['no_ijazah2'] ;?>. Kompetensi <?=$data['dokter']['kompetensi2'] ;?>
                        <?php endif; ?>
                        </td>
                    </tr>
                    <tr><th>KTA IDI</th><td>Nomor: <?=$data['dokter']['no_kta'] ;?> berlaku sampai <?=$tg1 . ' / '.$bl1.' / '.$th1; ?></td></tr>

                    <tr><th>Surat Tanda Rekomendasi</th><td>Nomor: <?=$data['dokter']['no_str'] ;?> berlaku sampai <?=$tg2 . ' / '.$bl2.' / '.$th2; ?></td></tr>
                    <tr><th>Status Keanggotaan IDI</th><td><?=$data['dokter']['status_idi'] ;?> </td></tr>
                    <tr><th>IDI Asal</th><td><?=$data['dokter']['idi_asal'] ;?> </td></tr>
                    <tr><th>Masuk IDI</th><td>Tahun <?=$data['dokter']['tahun_masuk'] ;?> </td></tr>
                    <tr><th>Keterangan lain</th><td><?=$data['dokter']['keterangan'] ;?> </td></tr>
                </tbody>
            </table>
        </div>        
    </div>
    <div class="row px-5">
      <div class="col-12">
        <div class="card">
          <div class="card-header bg-success">
            <h3>DATA PRAKTIK</h3>
          </div>
          <div class="card-body">
            <div class="row">

              <div class="col-4">
                <div class="card">
                  <div class="card-header bg-primary">
                    <h5>PRAKTIK 1</h5>
                  </div>
                  <div class="card-body bg-light">
                    <p>Alamat:<br><?=$data['praktik']['sip1'];?></p>
                    <p>Nomor SIP:<br><?=$data['praktik']['nosip1'];?></p>
                  </div>
                </div>
              </div>

              <div class="col-4">
                <div class="card">
                  <div class="card-header bg-primary">
                    <h5>PRAKTIK 2</h5>
                  </div>
                  <div class="card-body bg-light">
                    <p>Alamat:<br><?=$data['praktik']['sip2'];?></p>
                    <p>Nomor SIP:<br><?=$data['praktik']['nosip2'];?></p>
                  </div>
                </div>
              </div>

              <div class="col-4">
                <div class="card">
                  <div class="card-header bg-primary">
                    <h5>PRAKTIK 3</h5>
                  </div>
                  <div class="card-body bg-light">
                    <p>Alamat:<br><?=$data['praktik']['sip3'];?></p>
                    <p>Nomor SIP:<br><?=$data['praktik']['nosip3'];?></p>
                  </div>
                </div>
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col text-center">
        <a href="#" class="btn btn-default print-hide" onClick=window.print()>Cetak</a>
      </div>
    </div>
</div> <!-- container -->
<!-- modal -->
<div class="modal" tabindex="-1" role="dialog" id="modalFDokter">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h5 class="modal-title">Form Dokter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=BASEURL;?>/dokter/faceOff" method="post" enctype="multipart/form-data">
        <div class="form-group">
        <label for="fdok_id">ID Dokter</label>
        <input type="text" name="fdok_id" id="fdok_id" class="form-control" readonly>
        </div>
        <div class="form-group">
        <label for="fdok_foto">Berkas Foto</label>
        <input type="file" name="fdok_foto" id="fdok_foto">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="<?=BASEURL;?>/js/dokIndex.js"></script>