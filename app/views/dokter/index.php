<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
    <h1 class="pageTitle">DATA DOKTER</h1>

    <div class="table-responsive">
      <div>
        <i class="fa fa-plus-square btn btn-success mb-1 mt-2 py-3" style='font-size: 18px;' data-toggle="modal" data-target="#modalDokter"> Dokter Baru</i>
        <i class="fa fa-print btn btn-primary mb-1 mt-2 py-3" style='font-size: 18px;' onclick=window.open("<?=BASEURL;?>/laporan/dokter")></i>
      </div>
      <table class="table table-striped table-sm">
        <thead>
          <tr class='bg-info'>
            <th>ID DOKTER</th>
            <th>NPA IDI</th>
            <th>Nama Lengkap</th>
            <th>Jenis Kelamin</th>
            <th>Alamat</th>
            <th>Instansi</th>
            <th>No. Telp</th>
            <th>Kontrol</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach($data['dokter'] as $dokter): ;?>
          <tr>
            <td><?=sprintf("%08d",$dokter['id_dokter']); ?></td>
            <td><?=$dokter['no_kta']; ?></td>
            <td><?=$dokter['nama']; ?></td>
            <td><?=$dokter['jk']; ?></td>
            <td><?=$dokter['alamat']; ?></td>
            <td><?=$dokter['instansi']; ?></td>
            <td><?=$dokter['hp']; ?></td>
            <td>
              <i class="fa fa-address-card-o btn btn-primary dok-info">&nbsp;</i>
              <i class="fa fa-edit btn btn-warning dok-edit">&nbsp;</i>
              <i class="fa fa-trash-o btn btn-danger dok-kick">&nbsp;</i>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>  
      </table>
    </div>
    </div>
  </div>
</div>
<!-- modal -->
<div class="modal" tabindex="-1" role="dialog" id="modalDokter">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h5 class="modal-title">Form Dokter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <small><i>Form berbingkai merah wajib diisi</i></small>
        <form action="<?=BASEURL;?>/dokter/save" method="post" enctype="multipart/form-data">
        <input type="hidden" name="dok_mod" id="dok_mod" value="baru">
          <div class="form-group row">
            <h5 class="form-label">DATA UMUM</h5>
          </div>
          <div class="form-group row">
            <label for="dok_id" class="col-sm-4">ID RECORD</label>
              <div class="col-sm-8">
                <input type="text" name="dok_id" id="dok_id" class="form-control" readonly >
              </div>
          </div>
          <!-- dok_nama -->
          <div class="form-group row">
            <label for="dok_nama" class="col-sm-4">Nama Lengkap</label>
            <div class="col-sm-8">
              <input type="text" name="dok_nama" id="dok_nama" class="form-control required" required >
            </div>
          </div>

          <!-- dok_tpLahir -->

          <div class="form-group row">
            <label for="dok_tpLahir" class="col-sm-4">Tempat Lahir</label>
            <div class="col-sm-8">
              <input type="text" name="dok_tpLahir" id="dok_tpLahir" class="form-control required" required >
            </div>
          </div>

          <!-- dok_tgLahir -->
          <div class="form-group row">
            <label for="dok_tgLahir" class="col-sm-4">Tanggal Lahir</label>
            <div class="col-sm-8">
              <input type="date" name="dok_tgLahir" id="dok_tgLahir" class="form-control required" required >
            </div>
          </div>
          
          <!-- dok_jkl -->
          <div class="form-group row">
            <label for="dok_jkl" class="col-sm-4">Pria/Wanita</label>
            <div class="col-sm-8">
              <select name="dok_jkl" id="dok_jkl" class="form-control required">
              <option value="Pria">Pria</option>
              <option value="Wanita">Wanita</option>
              </select>
            </div>
          </div>

          <!-- dok_alamat -->
          <div class="form-group row">
            <label for="dok_alamat" class="col-sm-4">Alamat</label>
            <div class="col-sm-8">
              <input type="text" name="dok_alamat" id="dok_alamat" class="form-control">
            </div>
          </div>

          <!-- dok_telp -->
          <div class="form-group row">
            <label for="dok_telp" class="col-sm-4">Nomor Telephone</label>
            <div class="col-sm-8">
              <input type="text" name="dok_telp" id="dok_telp" class="form-control">
            </div>
          </div>

          <!-- dok_instansi -->
          <div class="form-group row">
            <label for="dok_instansi" class="col-sm-4">Instansi</label>
            <div class="col-sm-8">
              <input type="text" name="dok_instansi" id="dok_instansi" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <h5 class="form-label">PENDIDIKAN KEDOKTERAN</h5>
          </div>

          <!-- dok_pend1 -->
          <div class="form-group row">
            <label for="dok_pend1" class="col-sm-4">Perguruan Tinggi</label>
            <div class="col-sm-8">
              <input type="text" name="dok_pend1" id="dok_pend1" class="form-control required" required >
            </div>
          </div>

          <!-- dok_ijazah1 -->
          <div class="form-group row">
            <label for="dok_ijazah1" class="col-sm-4">Nomor Ijazah</label>
            <div class="col-sm-8">
              <input type="text" name="dok_ijazah1" id="dok_ijazah1" class="form-control required" required >
            </div>
          </div>

          <!-- dok_lulus1 -->
          <div class="form-group row">
            <label for="dok_lulus1" class="col-sm-4">Tahun Lulus</label>
            <div class="col-sm-8">
              <input type="number" name="dok_lulus1" id="dok_lulus1" class="form-control required" required >
            </div>
          </div>

          <!-- dok_kompet1 -->
          <div class="form-group row">
            <label for="dok_kompet1" class="col-sm-4">Kompetensi</label>
            <div class="col-sm-8">
              <input type="text" name="dok_kompet1" id="dok_kompet1" class="form-control required" required >
            </div>
          </div>

          <div class="form-group row">
            <h5 class="form-label">PENDIDIKAN SPESIALIS</h5>
          </div>

          <!-- dok_pend2 -->
          <div class="form-group row">
            <label for="dok_pend2" class="col-sm-4">Perguruan Tinggi</label>
            <div class="col-sm-8">
              <input type="text" name="dok_pend2" id="dok_pend2" class="form-control">
            </div>
          </div>

          <!-- dok_ijazah2 -->
          <div class="form-group row">
            <label for="dok_ijazah2" class="col-sm-4">Nomor Ijazah</label>
            <div class="col-sm-8">
              <input type="text" name="dok_ijazah2" id="dok_ijazah2" class="form-control">
            </div>
          </div>

          <!-- dok_lulus2 -->
          <div class="form-group row">
            <label for="dok_lulus2" class="col-sm-4">Tahun Lulus</label>
            <div class="col-sm-8">
              <input type="number" name="dok_lulus2" id="dok_lulus2" class="form-control">
            </div>
          </div>

          <!-- dok_kompet2 -->
          <div class="form-group row">
            <label for="dok_kompet2" class="col-sm-4">Kompetensi</label>
            <div class="col-sm-8">
              <input type="text" name="dok_kompet2" id="dok_kompet2" class="form-control">
            </div>
          </div>
          
          <div class="form-control-row">
            <h5 class="form-label">Kartu Tanda Anggota IDI</h5>
          </div>
          <!-- dok_kta -->
          <div class="form-group row">
            <label for="dok_kta" class="col-sm-4">Nomor</label>
            <div class="col-sm-8">
              <input type="text" name="dok_kta" id="dok_kta" class="form-control">
            </div>
          </div>

          <!-- dok_ktavalid -->
          <div class="form-group row">
            <label for="dok_ktavalid" class="col-sm-4">Berlaku Sampai</label>
            <div class="col-sm-8">
              <input type="date" name="dok_ktavalid" id="dok_ktavalid" class="form-control">
            </div>
          </div>

          <div class="form-group row">
          <h5 class="form-label">Surat Tanda Registrasi</h5>
          </div>
          <!-- dok_str -->
          <div class="form-group row">
            <label for="dok_str" class="col-sm-4">Nomor</label>
            <div class="col-sm-8">
              <input type="text" name="dok_str" id="dok_str" class="form-control">
            </div>
          </div>

          <!-- dok_strvalid -->
          <div class="form-group row">
            <label for="dok_strvalid" class="col-sm-4">Berlaku Sampai</label>
            <div class="col-sm-8">
              <input type="date" name="dok_strvalid" id="dok_strvalid" class="form-control">
            </div>
          </div>
          
          <div class="form-group row">
          <h5 class="form-label">Keanggotaan IDI</h5>
          </div>
          <!-- dok_stidi -->
          <div class="form-group row">
            <label for="dok_stidi" class="col-sm-4">Status</label>
            <div class="col-sm-8">
              <input type="text" name="dok_stidi" id="dok_stidi" class="form-control">
            </div>
          </div>

          <!-- dok_idiasal -->
          <div class="form-group row">
            <label for="dok_idiasal" class="col-sm-4">IDI asal</label>
            <div class="col-sm-8">
              <input type="text" name="dok_idiasal" id="dok_idiasal" class="form-control">
            </div>
          </div>

          <!-- dok_nridi -->
          <div class="form-group row">
            <label for="dok_nridi" class="col-sm-4">No. Rekomendasi IDI</label>
            <div class="col-sm-8">
              <input type="text" name="dok_nridi" id="dok_nridi" class="form-control">
            </div>
          </div>

          <!-- dok_thmasuk -->
          <div class="form-group row">
            <label for="dok_thmasuk" class="col-sm-4">Tahun Masuk</label>
            <div class="col-sm-8">
              <input type="text" name="dok_thmasuk" id="dok_thmasuk" class="form-control">
            </div>
          </div>

          <!-- dok_note -->
          <div class="form-group row">
            <label for="dok_note" class="col-sm-4">Keterangan Lain</label>
            <div class="col-sm-8">
              <input type="text" name="dok_note" id="dok_note" class="form-control">
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="<?=BASEURL;?>/js/dokIndex.js"></script>