<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
    <h1 class="pageTitle">DATA MUTASI DOKTER</h1>
    </div>
  </div> <!-- row -->
  <div class="row">
    <div class="col-lg-12">
    <i class="fa fa-print btn btn-primary mb-1 mt-2 py-3" style='font-size: 18px;' onclick=window.open("<?=BASEURL;?>/laporan/mutasi")></i>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr class='bg-success'>
              <th>ID Mutasi</th>
              <th>No. Rekomendasi</th>
              <th>Tanggal</th>
              <th>Nama Dokter</th>
              <th>Tujuan</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($data['mutasi'] AS $mutasi ): ?>
          <tr>
            <td><?=$mutasi['id_mutasi'];?></td>
            <td><?=$mutasi['norek'];?></td>
            <td><?=$this->ymd2dmy($mutasi['tgl_mutasi']);?></td>
            <td><?=$mutasi['nama'];?></td>
            <td><?=$mutasi['idi_tujuan'];?></td>
          </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>  
    </div>  
  </div> <!-- row -->
</div> <!-- container -->
