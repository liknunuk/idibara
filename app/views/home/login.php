<div id="lg_wrapper">
    <div id="lg_header">
    
    </div>
    <div id="lg_form">
        <form action="<?=BASEURL;?>/Pusdata/uauth" method="post">
            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="lg_lbuname">Username</span>
                    </div>
                    <input type="text" class="form-control"  aria-label="Username" aria-describedby="lg_lbuname" name="username">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="lg_password">Password</span>
                    </div>
                    <input type="password" class="form-control" aria-label="Password" aria-describedby="lg_password" name="password">
                </div>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-secondary px-5">Login</button>
            </div>
        </form>
    </div>
    <div id="lg_footer">
    <h6>IKATAN DOKTER INDONESIA<br>
    KABUPATEN BANJARNEGARA</h6>
    </div>
</div>