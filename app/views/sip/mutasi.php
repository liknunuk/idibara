<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <h1 class="pageTitle">PENERBITAN SURAT MUTASI</h1>
      <div class="col-8 mx-auto py-3 bg-litledark">
          <form action="<?=BASEURL;?>/surat/save/mutasi" method="post">
            <div class="form-group row">
                <label for="mut_noSurat" class="col-4">Nomor Surat</label>
                <div class="col-8">
                    <input type="text" name="mut_noSurat" id="mut_noSurat" class="form-control" value="/CAB.BNA/A.3/<?=date('m').'/'.date('Y');?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="sip_dokId" class="col-4">ID Dokter</label>
                <div class="col-8">
                    <input type="text" name="sip_dokId" id="sip_dokId" class="form-control" list="sip_dokList" placeholder="Tulis nama dokter">
                    <datalist id="sip_dokList"></datalist>
                </div>
            </div>
            <div class="form-group" id="infoIuran"></div>
            <div class="form-group row">
                <label for="mut_jabat" class="col-4">Jabatan dalam IDI</label>
                <div class="col-8">
                    <input type="text" name="mut_jabat" id="mut_jabat" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="mut_tujuan" class="col-4">IDI Tujuan</label>
                <div class="col-8">
                    <input type="text" name="mut_tujuan" id="mut_tujuan" class="form-control">
                </div>
            </div>

            <div class="form-group row">
              <div class="col-12 text-right">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>

          </form>

      </div>
    </div>
  </div>
</div>
<?php #echo assetDir; ?>
<script src="<?=BASEURL;?>/js/sipIndex.js"></script>