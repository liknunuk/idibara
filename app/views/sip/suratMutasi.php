<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SIP LOKAL</title>
    <style>
        h1,h2,h3,h4,p{padding: 0; margin: 0;}
        .fz-10 { font-size: 10pt;}
        p{text-align: justify;}
        p.aj{text-align: justify;}
        @media print{
        h1,h2,h3,h4,p{padding: 0; margin: 0;}
        .fz-10 { font-size: 10pt;}
        p.aj{text-align: justify;}
        }
        @media print{
            td { font-size: 11pt; }
        }
    </style>
</head>
<body onload=window.print()>
    <?php /*
        echo "<pre>";
        print_r($data['ketua']);
        echo "<hr>";
        print_r($data['sip']);
        echo "<hr>";
        print_r($data['dokter']);
        echo "</pre>"; */
    ?>
<table width="100%">
    <tr>
        <td width="150px">
            <img src="<?=BASEURL?>/img/logo-idi.png"  style="width: 144px; margin:auto;" alt="idi bara">
        </td>
        <td align="center">
<h1>IKATAN DOKTER INDONESIA</h1>
<h3>(THE INDONESIAN MEDICAL ASSOCIATION)<br>
IDI CABANG BANJARNEGARA</h3>
<p class="fz-10">Alamat Sekretariat : Kantor Dinas Kesehatan Kabupaten Banjarnegara<br>
Jl. Selamanik No.8, Kelurahan Semampir – Banjarnegara Kode Pos 53418<br>
Telp. / WA : (0286) 591080 / 0853 2509 1551 @mail : idi_bna@yahoo.com / Website : www.idibanjarnegara.com</p>
        </td>
    </tr>
</table>    
<table width="100%" border="0" cellspacing="0">
    <tbody>
        <tr><td colspan="2" align="center"><h2>SURAT KETARANGAN PINDAH KEANGGOTAAN IDI</h2></td></tr>  
        <tr><td colspan="2" align="center"><h4>Nomor : <?=$data['sip']['mut_noSurat'];?></h4></td></tr>    
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr><td colspan="2">Yang bertanda tangan dibawah ini, Pengurus Ikatan Dokter Indonesia Cabang Banjarnegara </td></tr>
        <tr><td width="300">Nama</td><td><?=$data['ketua']['nama'];?></td></tr>
        <tr><td>NPA IDI</td><td><?=$data['ketua']['no_kta'];?></td></tr>
        <tr><td>Jabatan</td><td>KETUA IDI Cabang Banjarnegara</td></tr> 
        <tr><td colspan="2">
        <p>Menerangkan Bahwa anggota IDI ­yang namanya tersebut di bawah ini akan pindah keanggotaannya dari IDI Cabang Banjarnegara dan akan mendaftarkan diri menjadi Anggota IDI <?=$data['sip']['mut_tujuan'];?>.</p>
        </td></tr>
        <tr><td>&nbsp;</td><td>
            <tr><td>Nama</td><td><?=$data['dokter']['nama'];?></td></tr> 
            <tr><td>Tempat dan Tanggal Lahir</td>
            <td>
                <?=$data['dokter']['tempat_lahir'];?>, 
                <?php 
            list($t,$b,$h)=explode("-" , $data['dokter']['tgl_lahir']); 
            echo "$h / $b / $t";
            ?>
        </td></tr>
        <?php
            if($data['dokter']['pt2'] == '' ){
                $profesi = $data['dokter']['kompetensi1'];
                $pt = $data['dokter']['pt1'];
                $ijz = $data['dokter']['no_ijazah1'];
                $lulus = $data['dokter']['tahun_lulus'];
            }else{
                $profesi = $data['dokter']['kompetensi2'];
                $pt = $data['dokter']['pt2'];
                $ijz = $data['dokter']['no_ijazah2'];
                $lulus = $data['dokter']['tahun_lulus2'];
            }
        ?>
        <tr><td>Kompetensi</td><td><?=$profesi;?></td></tr> 
        <tr><td>Jenis Kelamin</td><td><?=$data['dokter']['jk'];?></td></tr> 
        <tr><td>Alamat</td><td><?=$data['dokter']['alamat'];?></td></tr> 
        <tr><td>Jabatan Dalam IDI</td><td><?=$data['sip']['mut_jabat'];?></td></tr> 
        <tr><td>NPA IDI</td><td><?=$data['dokter']['no_kta'];?></td></tr> 
        <tr><td>Nomor STR</td><td><?=$data['dokter']['no_str'];?></td></tr> 
        <tr><td>Masa Berlaku STR</td><td>
            <?php 
            list($t,$b,$h)=explode("-" , $data['dokter']['berlaku_str']); 
            echo "$h / $b / $t";
            ?>
        </td></tr> 
        <tr><td>Alamat Tujuan</td><td><?=$data['sip']['mut_tujuan'];?></td></tr> 
        <tr><td>&nbsp;</td><td>
        <tr><td colspan="2">
            <p class="aj">
            Di samping itu yang bersangkutan selama ini  :<br/>
            1. Telah lunas iuran anggota IDI Cabang Banjarnegara<br/>
            2. Telah melaksanakan semua kewajiban organisasi dengan baik<br/>
            3. Tidak pernah terlibat pelanggaran etik kedokteran atau mal praktik<br/>
            4. Tidak sedang menjalani sanksi organisasi IDI<br/>
            5. Tidak pernah terkena sanksi organisasi IDI
            </p>
        </td></tr>
        
        <tr><td colspan="2"><p class="aj">Demikian surat keterangan ini kami buat agar di jadikan maklum, atas kerjasamanya diucapkan terima kasih.</p>.&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>
        <div style="width: 400px; float:right; text-align:center;">
        <br/><br/>
                Banjarnegara, <?=date('d F Y');?><br/>
IDI Cabang Banjarnegara<br/>
Ketua
<br><br><br>
(<?=$data['ketua']['nama'];?>)<br/>
NPA. <?=$data['ketua']['id_dokter'] ;?>
        </div>
        </td></tr>
    </tbody>
</table>

</body>
</html>