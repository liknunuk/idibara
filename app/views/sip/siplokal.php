<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <h1 class="pageTitle">PENERBITAN SIP REGULER</h1>
      <div class="col-8 mx-auto py-3 bg-litledark">
          <form action="<?=BASEURL;?>/surat/save/reguler" method="post">
            <div class="form-group row">
                <label for="reg_noSurat" class="col-4">Nomor Surat</label>
                <div class="col-8">
                    <input type="text" name="reg_noSurat" id="reg_noSurat" class="form-control" value="/CAB.BNA/A.3/<?=date('m').'/'.date('Y');?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="sip_dokId" class="col-4">ID Dokter</label>
                <div class="col-8">
                    <input type="text" name="sip_dokId" id="sip_dokId" class="form-control" list="sip_dokList" placeholder="Tulis nama dokter">
                    <datalist id="sip_dokList"></datalist>
                </div>
            </div>
            <div class="form-group" id="infoIuran"></div>
            <div class="form-group row">
                <label for="reg_sipke" class="col-4">SIP Ke</label>
                <div class="col-8">
                    <input type="number" name="reg_sipke" id="reg_sipke" class="form-control" min=1 max=3 value=1>
                </div>
            </div>
            <!-- sip yg diajukan -->
            <div class="form-group row">
              <div class="col-12">
                <h5 style="text-align:center; background-color: #888; padding: 5px; color: white;">Surat Ijin Praktik yang Diajukan</h5>
              </div>
            </div>
            <div class="form-group row">
                <label for="reg_tempat1" class="col-4">Nama Tempat Praktik</label>
                <div class="col-8">
                    <input type="text" name="reg_tempat1" id="reg_tempat1" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="reg_alamat1" class="col-4">Alamat </label>
                <div class="col-8">
                    <input type="text" name="reg_alamat1" id="reg_alamat1" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="reg_jadwal1" class="col-4">Jadwal Praktik</label>
                <div class="col-8">
                    <input type="text" name="reg_jadwal1" id="reg_jadwal1" class="form-control">
                </div>
            </div>

            <!-- sip yg dimiliki -->
            <div class="form-group row">
              <div class="col-12">
                <h5 style="text-align:center; background-color: #888; padding: 5px; color: white;">Surat Ijin Praktik yang Dimiliki</h5>
              </div>
            </div>
            <div class="form-group row">
                <label for="reg_tempat2" class="col-4">Nama Tempat Praktik (1)</label>
                <div class="col-8">
                    <input type="text" name="reg_tempat2" id="reg_tempat2" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="reg_alamat2" class="col-4">Alamat </label>
                <div class="col-8">
                    <input type="text" name="reg_alamat2" id="reg_alamat2" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="reg_jadwal2" class="col-4">Jadwal Praktik</label>
                <div class="col-8">
                    <input type="text" name="reg_jadwal2" id="reg_jadwal2" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="reg_tempat3" class="col-4">Nama Tempat Praktik (2)</label>
                <div class="col-8">
                    <input type="text" name="reg_tempat3" id="reg_tempat3" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="reg_alamat3" class="col-4">Alamat </label>
                <div class="col-8">
                    <input type="text" name="reg_alamat3" id="reg_alamat3" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="reg_jadwal3" class="col-4">Jadwal Praktik</label>
                <div class="col-8">
                    <input type="text" name="reg_jadwal3" id="reg_jadwal3" class="form-control">
                </div>
            </div>

            <div class="form-group row">
              <div class="col-12 text-right">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>

          </form>

      </div>
    </div>
  </div>
</div>
<?php #echo assetDir; ?>
<script src="<?=BASEURL;?>/js/sipIndex.js"></script>