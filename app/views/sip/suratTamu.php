<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SIP LOKAL</title>
    <style>
        h1,h2,h3,h4,p{padding: 0; margin: 0;}
        .fz-10 { font-size: 10pt;}
        p{text-align: justify;}
        p.aj{text-align: justify;}
        @media print{
        h1,h2,h3,h4,p{padding: 0; margin: 0;}
        .fz-10 { font-size: 10pt;}
        p.aj{text-align: justify;}
        }
    </style>
</head>
<body onload=window.print()>
    <?php /*
        echo "<pre>";
        print_r($data['ketua']);
        echo "<hr>";
        print_r($data['sip']);
        echo "<hr>";
        print_r($data['dokter']);
        echo "</pre>"; */
    ?>
<table width="100%">
    <tr>
        <td width="150px">
            <img src="<?=BASEURL?>/img/logo-idi.png"  style="width: 144px; margin:auto;" alt="idi bara">
        </td>
        <td align="center">
<h1>IKATAN DOKTER INDONESIA</h1>
<h3>(THE INDONESIAN MEDICAL ASSOCIATION)<br>
IDI CABANG BANJARNEGARA</h3>
<p class="fz-10">Alamat Sekretariat : Kantor Dinas Kesehatan Kabupaten Banjarnegara<br>
Jl. Selamanik No.8, Kelurahan Semampir – Banjarnegara Kode Pos 53418<br>
Telp. / WA : (0286) 591080 / 0853 2509 1551 @mail : idi_bna@yahoo.com / Website : www.idibanjarnegara.com</p>
        </td>
    </tr>
</table>    
<table width="100%" border="0" cellspacing="0">
    <tbody>
        <tr><td colspan="2" align="center"><h2>SURAT REKOMENDASI IJIN PRAKTIK</h2></td></tr>    
        <tr><td colspan="2" align="center"><h2>( DOKTER TAMU )</h2></td></tr>    
        <tr><td colspan="2" align="center"><h4>Nomor : <?=$data['sip']['tmu_noSurat'];?></h4></td></tr>    
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr><td colspan="2">Yang bertanda tangan dibawah ini, Pengurus Ikatan Dokter Indonesia Cabang Banjarnegara </td></tr>
        <tr><td width="300">Nama</td><td><?=$data['ketua']['nama'];?></td></tr>
        <tr><td>NPA IDI</td><td><?=$data['ketua']['no_kta'];?></td></tr>
        <tr><td>Jabatan</td><td>KETUA IDI Cabang Banjarnegara</td></tr> 
        <tr><td colspan="2">Menerangkan Bahwa :</td></tr>
        <tr><td>&nbsp;</td><td>
            <tr><td>Nama</td><td><?=$data['dokter']['nama'];?></td></tr> 
            <tr><td>Tempat dan Tanggal Lahir</td>
            <td>
                <?=$data['dokter']['tempat_lahir'];?>, 
                <?php 
            list($t,$b,$h)=explode("-" , $data['dokter']['tgl_lahir']); 
            echo "$h / $b / $t";
            ?>
    </td></tr> 
    <tr><td>Alamat</td><td><?=$data['dokter']['alamat'];?></td></tr> 
    <tr><td>Pekerjaan</td><td><?=$data['sip']['tmu_kerja'];?></td></tr> 
    <?php
            if($data['dokter']['pt2'] == '' ){
                $profesi = $data['dokter']['kompetensi1'];
                $pt = $data['dokter']['pt1'];
                $ijz = $data['dokter']['no_ijazah1'];
                $lulus = $data['dokter']['tahun_lulus'];
            }else{
                $profesi = $data['dokter']['kompetensi2'];
                $pt = $data['dokter']['pt2'];
                $ijz = $data['dokter']['no_ijazah2'];
                $lulus = $data['dokter']['tahun_lulus2'];
            }
            ?>
        <tr><td>Profesi</td><td><?=$profesi;?></td></tr> 
        <tr><td>Perguruan Tinggi</td><td><?=$pt;?></td></tr> 
        <tr><td>Nomor / Tahun Lulus</td><td><?=$ijz.' / '.$lulus;?></td></tr> 
        <tr><td>IDI Asal</td><td><?=$data['dokter']['idi_asal'];?></td></tr> 
        <tr><td>Nomor STR</td><td><?=$data['dokter']['no_str'];?></td></tr> 
        <tr><td>Masa Berlaku STR</td><td>
            <?php 
            list($t,$b,$h)=explode("-" , $data['dokter']['berlaku_str']); 
            echo "$h / $b / $t";
            ?>
        </td></tr> 
        <tr><td>&nbsp;</td><td>
        <tr><td colspan="2">
            <p class="aj">Adalah anggota IDI Cabang <?=$data['dokter']['idi_asal'];?> Berdasarkan Rekomendasi Nomor : <?=$data['sip']['tmu_rekom'];?> yang menyatakan bahwa yang bersangkutan adalah anggota IDI Cabang <?=$data['dokter']['idi_asal'];?> dan tidak berkeberatan untuk memberikan pelayanan kesehatan di Wilayah IDI Cabang Banjarnegara dan yang bersangkutan bersedia memenuhi kewajiban sebagai anggota sebagaimana yang tercantum dalam Anggaran Dasar dan Anggara Rumah Tanggan Ikatan Dokter Indonesia.</p>
            <p class="aj ">Berdasarkan pengamatan dan pengetahuan kami terhadap kemampuan ilmu kedokteran dan pelaksanaan Kode Etik Kedokteran Indonesia, maka kami tidak keberatan bilamana kepada yang bersangkutan diberikan Surat Rekomendasi Ijin Praktik ke <?=$data['sip']['tmu_sipke'];?> di alamat :</p>
        </td></tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr><td colspan="2">Surat Ijin Praktik yang diajukan :</td></tr>
        <tr><td>Nama Tempat Praktik</td><td><?=$data['sip']['tmu_tempat1'];?></td></tr>
        <tr><td>Alamat</td><td><?=$data['sip']['tmu_alamat1'];?></td></tr>
        <tr><td>Jadwal Praktik</td><td><?=$data['sip']['tmu_jadwal1'];?></td></tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr><td colspan="2">Surat Ijin Praktik yang telah dimiliki / diterbitkan :</td></tr>
        <tr><td>Nama Tempat Praktik (1)</td><td><?=$data['sip']['tmu_tempat2'];?></td></tr>
        <tr><td>Alamat</td><td><?=$data['sip']['tmu_alamat2'];?></td></tr>
        <tr><td>Jadwal Praktik</td><td><?=$data['sip']['tmu_jadwal2'];?></td></tr>
        <tr><td>Nama Tempat Praktik (2)</td><td><?=$data['sip']['tmu_tempat3'];?></td></tr>
        <tr><td>Alamat</td><td><?=$data['sip']['tmu_alamat3'];?></td></tr>
        <tr><td>Jadwal Praktik</td><td><?=$data['sip']['tmu_jadwal3'];?></td></tr>
        <tr><td colspan="2">Demikian surat rekomendasi ini disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih.&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>
        <div style="width: 400px; float:right; text-align:center;">
                Banjarnegara, <?=date('d F Y');?><br/>
IDI Cabang Banjarnegara<br/>
Ketua
<br><br><br>
(<?=$data['ketua']['nama'];?>)<br/>
NPA. <?=$data['ketua']['id_dokter'] ;?>
        </div>
        </td></tr>
    </tbody>
</table>

</body>
</html>