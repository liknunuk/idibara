<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <h1 class="pageTitle">DATA PENERBITAN REKOMENDASI</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
    <i class="fa fa-print btn btn-primary mb-1 mt-2 py-3" style='font-size: 18px;' onclick=window.open("<?=BASEURL;?>/laporan/surat")></i>
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr class='bg-success'>
              <th>ID Surat</th>
              <th>Jenis Surat</th>
              <th>Nomor Surat</th>
              <th>Tanggal Surat</th>
              <th>Nama Dokter</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($data['arsip'] AS $arsip): ?>
            <tr>
              <td><?=$arsip['id_surat'];?></td>
              <td><?=$arsip['tipeSurat'];?></td>
              <td><?=$arsip['nomorSurat'];?></td>
              <td><?=$arsip['tanggalSurat'];?></td>
              <td><?=$arsip['nama'];?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
