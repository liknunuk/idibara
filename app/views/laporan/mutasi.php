<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$data['judul'];?></title>
</head>
<body onload=window.print()>
    <h3>DATA LAPORAN <?=$data['subjek'];?> BULAN <?=$data['bulan'];?></h3>
    <table width="100%" border="1" cellspacing="0">
        <thead>
            <tr>
                <!-- ID Mutasi	No. Rekomendasi	Tanggal	Nama Dokter	Tujuan -->
                <th>ID MUTASI</th>
                <th>NO. REKOMENDASI</th>
                <th>TANGGAL</th>
                <th>NAMA DOKTER</th>
                <th>IDI TUJUAN</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($data['mutasi'] AS $mutasi ): ?>
            <tr>
                <td><?=$mutasi['id_mutasi'];?></td>
                <td><?=$mutasi['norek'];?></td>
                <td align="right"><?=$this->ymd2dmy($mutasi['tgl_mutasi']);?></td>
                <td><?=$mutasi['nama'];?></td>
                <td><?=$mutasi['idi_tujuan'];?></td>
            </tr>
        <?php endforeach;  ?>
           
        </tbody>
    </table>
    <p>Jumlah: <?=$data['jumlah'];?> Orang</p>
    
</body>
</html>