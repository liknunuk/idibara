<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$laporan['judul'];?></title>
</head>
<body onload=window.print()>
    <h3>DATA LAPORAN <?=$data['subjek'];?></h3>
    <table width="100%" border="1" cellspacing="0">
        <thead>
            <tr>
                <th>NPA IDI</th>
                <th>NAMA DOKTER</th>
                <th>PRIA / WANITA</th>
                <th>TEMPAT DAN TANGGAL LAHIR</th>
                <th>KOMPETENSI</th>
                <th>MASA BERLAUKU KTA</th>
            </tr>
        </thead>
        <tbody>
        <?php  foreach($data['dokter'] AS $dokter ): ?>
            <tr>
                <td><?=$dokter['no_kta'];?></td>
                <td><?=$dokter['nama'];?></td>
                <td><?=$dokter['jk'];?></td>
                <td>
                    <?=$dokter['tempat_lahir'].', '.$dokter['tgl_lahir'];?>
                </td>
                <td>
                    <?=$dokter['kompetensi1'].' - '.$dokter['pt1'];?><br>
                    <?=$dokter['kompetensi2'].' - '.$dokter['pt2'];?>
                </td>
                <td><?=$dokter['kta_valid'];?></td>
            </tr>
        <?php endforeach;  ?>
        </tbody>
    </table>
    <p>Jumlah: <?=$data['jumlah'];?> Orang</p>
    
</body>
</html>