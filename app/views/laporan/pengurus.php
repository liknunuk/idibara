<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$data['title'];?></title>
</head>

<body onload=window.print()>

    <h3 style="text-align:center;">DATA <?=$data['subjek'];?> PERIODE <?=$data['lastperiod'];?></h3>
    <table border="1" cellspacing=0 width="50%" align="center">
          <thead>
            <tr>
              <th>Jabatan</th>
              <th>Nama Dokter</th>
            </tr>
          </thead>
          <tbody>
            <?php $jabatan = $data['jabatan']; foreach($data['pengurus'] AS $pengurus): ?>
            
            <?php  $ijab = $pengurus['jabatan']; ?>
            <tr>
              <td><?=$jabatan[$ijab];?></td>
              <td><?=$pengurus['dokter'];?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
    
</body>
</html>