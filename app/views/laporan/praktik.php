<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$data['title'];?></title>
</head>

<body onload=window.print()>

    <h3 style="text-align:center;">LAPORAN DATA <?=$data['subjek'];?> </h3>
    <table border="1" cellspacing=0 width="100%" align="center">
          <thead>
            <tr>
              <th>ID PRAKTIK</th>
              <th>Nama Dokter</th>
              <th>Praktik 1</th>
              <th>Praktik 2</th>
              <th>Praktik 3</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach( $data['praktik'] as $praktik ): ?>
            <tr>
                <td><?=$praktik['id_praktik'];?></td>
                <td><?=$praktik['dokter'];?></td>
                <td><?=$praktik['sip1'];?><br>No. SIP: <?=$praktik['nosip1'];?></td>
                <td><?=$praktik['sip2'];?><br>No. SIP: <?=$praktik['nosip2'];?></td>
                <td><?=$praktik['sip3'];?><br>No. SIP: <?=$praktik['nosip3'];?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
</body>
</html>