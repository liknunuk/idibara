<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$data['title'];?></title>
</head>

<body onload=window.print()>
    <h3 style="text-align:center;">LAPORAN DATA <?=$data['subjek'];?> </h3>
    <table border="1" cellspacing=0 width="100%" align="center">
          <thead>
            <tr>
              <th>ID Surat</th>
              <th>Nomor Surat</th>
              <th>Tipe Surat</th>
              <th>Tanggal Surat</th>
              <th>Nama Dokter</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach( $data['arsip'] AS $surat ): ?>
            <tr>
                <td><?=$surat['id_surat'];?></td>
                <td><?=$surat['nomorSurat'];?></td>
                <td><?=$surat['tipeSurat'];?></td>
                <td><?=$surat['tanggalSurat'];?></td>
                <td><?=$surat['nama'];?></td>
            </tr>
            <?php endforeach; ?>

          </tbody>
        </table>
</body>
</html>