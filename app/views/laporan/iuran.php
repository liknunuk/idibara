<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$data['judul'];?></title>
</head>
<body onload=window.print()>
    <h3>DATA LAPORAN <?=$data['subjek'];?> BULAN <?=$data['bulan'];?></h3>
    <table width="100%" border="1" cellspacing="0">
        <thead>
            <tr>
                <!-- tanggal,nama,untuk_bayar,nominal -->
                <th>TANGGAL</th>
                <th>NAMA DOKTER</th>
                <th>UNTUK PEMBAYARAN</th>
                <th>JUMLAH</th>
            </tr>
        </thead>
        <tbody>
        <?php  $sum = 0; foreach($data['iuran'] AS $iuran ): ?>
            <tr>
                <td><?=$iuran['tanggal'];?></td>
                <td><?=$iuran['nama'];?></td>
                <td><?=$iuran['untuk_bayar'];?></td>
                <td align='right'><?=number_format($iuran['nominal'],0,',','.');?></td>
            </tr>
        <?php $sum+= $iuran['nominal']; endforeach;  ?>
            <tr>
                <td colspan="3">Jumlah Total</td>
                <td align='right'><?=number_format($sum,0,',','.');?></td>
            </tr>
        </tbody>
    </table>
    <p>Jumlah: <?=$data['jumlah'];?> Orang</p>
    
</body>
</html>