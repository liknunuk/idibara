<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <h1 class="pageTitle">DATA PRAKTIK DOKTER</h1>
      <div class="table-responsive py-5">
      <div>
        <i class="fa fa-plus-square btn btn-success mb-1 mt-2 py-3" id="sip_add" style='font-size: 18px;' data-toggle="modal" data-target="#modalPraktik"> Praktik Baru</i>
        <i class="fa fa-print btn btn-primary mb-1 mt-2 py-3" style='font-size: 18px;' onclick=window.open("<?=BASEURL;?>/laporan/praktik")></i>
      </div>
        <table class="table table-hover table-sm">
          <thead>
            <tr class='bg-success'>
              <th>ID DOKTER</th>
              <th>NAMA DOKTER</th>
              <th>PRAKTIK 1</th>
              <th>PRAKTIK 2</th>
              <th>PRAKTIK 3</th>
              <th>KETERANGAN</th>
              <th>KONTROL</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($data['praktik'] as $praktik): ?>
          <tr>
            <td><?=$praktik['id_praktik']; ?></td>
            <td><?=$praktik['dokter']; ?></td>
            <td><?=$praktik['sip1'] . '<br>' . $praktik['nosip1']; ?></td>
            <td><?=$praktik['sip2'] . '<br>' . $praktik['nosip2']; ?></td>
            <td><?=$praktik['sip3'] . '<br>' . $praktik['nosip3']; ?></td>
            <td><?=$praktik['keterangan']; ?></td>
            <td>
              <i class="fa fa-edit btn btn-warning sip-edit"></i>
              <i class="fa fa-trash-o btn btn-danger sip-kick"></i>
            </td>
          </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- modal -->
<div class="modal" tabindex="-1" role="dialog" id="modalPraktik">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h5 class="modal-title">Form Praktik</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <small><i>Form berbingkai merah wajib diisi</i></small>
        <form action="<?=BASEURL;?>/praktik/save" method="post">
        <input type="hidden" name="sip_id" id="sip_id">
        <input type="hidden" name="sip_mod" id="sip_mod">
        <div class="form-group row">
          <label for="sip_dokId" class="col-sm-4">ID Dokter</label>
          <div class="col-sm-8">
            <input type="text" name="sip_dokId" id="sip_dokId" class="form-control required" required list="sip_dokList" placeholder="Tuliskan Nama Dokter">
            <datalist id="sip_dokList"></datalist>
          </div>
        </div>

        <div class="form-group row">
          <label for="sip_alamat1" class="col-sm-4">Alamat Praktik 1</label>
          <div class="col-sm-8">
            <input type="text" name="sip_alamat1" id="sip_alamat1" class="form-control" >
          </div>
        </div>

        <div class="form-group row">
          <label for="sip_nomor1" class="col-sm-4">Nomor SIP 1</label>
          <div class="col-sm-8">
            <input type="text" name="sip_nomor1" id="sip_nomor1" class="form-control" >
          </div>
        </div>

        <div class="form-group row">
          <label for="sip_alamat2" class="col-sm-4">Alamat Praktik 2</label>
          <div class="col-sm-8">
            <input type="text" name="sip_alamat2" id="sip_alamat2" class="form-control" >
          </div>
        </div>

        <div class="form-group row">
          <label for="sip_nomor2" class="col-sm-4">Nomor SIP 2</label>
          <div class="col-sm-8">
            <input type="text" name="sip_nomor2" id="sip_nomor2" class="form-control" >
          </div>
        </div>

        <div class="form-group row">
          <label for="sip_alamat3" class="col-sm-4">Alamat Praktik 3</label>
          <div class="col-sm-8">
            <input type="text" name="sip_alamat3" id="sip_alamat3" class="form-control" >
          </div>
        </div>

        <div class="form-group row">
          <label for="sip_nomor3" class="col-sm-4">Nomor SIP 3</label>
          <div class="col-sm-8">
            <input type="text" name="sip_nomor3" id="sip_nomor3" class="form-control" >
          </div>
        </div>

        <div class="form-group row">
          <label for="sip_note" class="col-sm-4">Keterangan</label>
          <div class="col-sm-8">
            <input type="text" name="sip_note" id="sip_note" class="form-control" >
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="<?=BASEURL;?>/js/sipIndex.js"></script>