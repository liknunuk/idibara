<?php $jabatan = $data['jabatan'];?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
    <h1 class="pageTitle">DATA PENGURUS IDI BANJARNEGARA</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="table-responsive px-5">
        <div class='mb-2'>
          <i class="fa fa-plus-square btn btn-success mb-1 mt-2 py-3" style='font-size: 18px;' data-toggle="modal" data-target="#modalIdi"> &nbsp;PENGURUS</i>
          <i class="fa fa-print btn btn-primary mb-1 mt-2 py-3" style='font-size: 18px;' onclick=window.open("<?=BASEURL;?>/laporan/pengurus")></i>
        </div>
        <table class="table table-sm">
          <thead>
            <tr class="bg-info">
              <th colspan="3" style="text-align:center; font-size:18px;">Periode:<?=$data['lastperiod'];?></th>
            </tr>
            <tr class="bg-primary">
              <th>Jabatan</th>
              <th>Nama Dokter</th>
              <th width="50">Konrol</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($data['pengurus'] AS $pengurus): ?>
            <?php $ijab = $pengurus['jabatan']; ?>
            <tr>
              <td><?=$jabatan[$ijab];?></td>
              <td><?=$pengurus['dokter'];?></td>
              <td><i class="fa fa-trash-o btn btn-danger" id="<?=$pengurus['id_pengurus'];?>">&nbsp;</i></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal" tabindex="-1" role="dialog" id="modalIdi">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Form Pengurus</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=BASEURL;?>/pengurus/save" method="post">
          <input type="hidden" name="idi_mod" id="idi_mod" value="baru">
          <input type="hidden" name="idi_idx" id="idi_idx">
          
          <div class="form-group row">
            <label for="idi_periode" class="col-sm-3">Periode Jabatan</label>
            <div class="col-sm-9">
              <input type="text" name="idi_periode" id="idi_periode" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <label for="idi_dokter" class="col-sm-3">ID Dokter</label>
            <div class="col-sm-9">
              <input type="text" name="idi_dokter" id="idi_dokter" class="form-control" placeholder="Tulis Nama Dokter" list="dokList">
              <datalist id="dokList"></datalist>
            </div>
          </div>

          <div class="form-group row">
            <label for="idi_jabatan" class="col-sm-3">Jabatan</label>
            <div class="col-sm-9">
              <select name="idi_jabatan" id="idi_jabatan" class="form-control">
              <?php
                  for($i = 1 ;  $i < COUNT($jabatan) ; $i++ ){
                    echo "<option value='".$i."'>".$jabatan[$i]."</option>";
                  }
              ?>
              </select>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="<?=BASEURL;?>/js/idiIndex.js"></script>