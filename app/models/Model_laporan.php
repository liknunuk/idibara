<?php
class Model_laporan
{
    
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function dokter(){
        $sql = "SELECT no_kta , nama , jk , tempat_lahir , DATE_FORMAT(tgl_lahir , '%d-%m-%Y') tgl_lahir , kompetensi1, pt1 , kompetensi2 , pt2 , DATE_FORMAT(berlaku_kta,'%d-%m-%Y') kta_valid FROM dokter ORDER BY nama";

        $this->db->query($sql);
        $this->db->execute();
        $data = $this->db->resultSet();
        $rows = $this->db->rowCount();

        return array ('hasil'=>$rows,'data'=>$data);
    }

    public function iuran($bulan){
        
        $sql = "SELECT DATE_FORMAT(tanggal_transaksi,'%d-%m-%Y') tanggal , dokter.nama , untuk_bayar , nominal FROM iuran , dokter WHERE tanggal_transaksi LIKE '{$bulan}%' && dokter.id_dokter = iuran.id_dokter";
        
        $this->db->query($sql);
        $this->db->execute();
        $data = $this->db->resultSet();
        $rows = $this->db->rowCount();
        return array ('hasil'=>$rows,'data'=>$data);
        
    }

    public function mutasi($bulan){
        
        $sql = "SELECT mutasi.* , dokter.nama FROM mutasi , dokter WHERE dokter.id_dokter = mutasi.id_dokter && tgl_mutasi LIKE '{$bulan}%' ORDER BY tgl_mutasi DESC LIMIT 40";
        $this->db->query($sql);
        $this->db->execute();
        $data = $this->db->resultSet();
        $rows = $this->db->rowCount();
        return array ('hasil'=>$rows,'data'=>$data);
        
    }

}
