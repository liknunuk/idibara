<?php
class Model_pengurus
{
    private $table = "pengurus";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function lastPeriod(){
        $sql = "SELECT MAX(periode) lper FROM pengurus";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function dftPengurus(){
        $period = $this->lastPeriod();
        $sql = "SELECT pengurus.* , dokter.nama dokter FROM {$this->table} , dokter WHERE periode = :period && dokter.id_dokter = pengurus.id_dokter ORDER BY jabatan";
        $this->db->query($sql);
        $this->db->bind("period",$period['lper']);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function addPengurus(){
        //echo "<pre>"; print_r($_POST); echo "</pre>";
        
        $sql = "INSERT INTO pengurus (periode,id_dokter,jabatan) VALUES (:periode , :id_dokter, :jabatan)";
        $this->db->query($sql);
        $this->db->bind('periode',$_POST['idi_periode']);
        $this->db->bind('id_dokter',$_POST['idi_dokter']);
        $this->db->bind('jabatan',$_POST['idi_jabatan']);
        $this->db->execute();
        header("Location:" . BASEURL . "/pengurus");
        
    }

    public function chgPengurus(){
        $sql = "UPDATA pengurus SET periode = :periode , id_dokter = : id_dokter , jabatan = :jabatan WHERE id_jabatan = :id_jabatan";

        $this->db->bind('periode',$_POST['idi_periode']);
        $this->db->bind('id_dokter',$_POST['idi_dokter']);
        $this->db->bind('jabatan',$_POST['idi_jabatan']);
        $this->db->bind('id_jabatan',$_POST['idi_idx']);
        $this->db->execute();
        header("Location:" . BASEURL . "/pengurus");
    }

    public function rmvPengurus($idx){
        $sql = "DELETE FROM pengurus WHERE id_pengurus = :idx LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idx',$idx);
        $this->db->execute();
    }

    public function ketuanya(){
        $periode = $this->lastPeriod();
        $peraktif = $periode['lper'];
        $sql = "SELECT pengurus.id_dokter, dokter.nama , dokter.no_kta FROM pengurus , dokter WHERE pengurus.jabatan = 2 && periode='{$peraktif}' && dokter.id_dokter = pengurus.id_dokter";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function bendaharanya(){
        $periode = $this->lastPeriod();
        $peraktif = $periode['lper'];
        $sql = "SELECT pengurus.id_dokter, dokter.nama , dokter.no_kta FROM pengurus , dokter WHERE pengurus.jabatan = 5 && periode='{$peraktif}' && dokter.id_dokter = pengurus.id_dokter";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function penguruskah($id){

        $periode = $this->lastPeriod();
        $periode = $periode['lper'];

        $sql = "SELECT count(jabatan) qty, jabatan from pengurus where id_dokter = '{$id}' && periode='{$periode}'";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function dftJabatan(){
        return array(
            "",
            "Dewan Penasehat ",
            "Ketua",
            "Wakil Ketua",
            "Sekretaris",
            "Bendahara",
            "Majelis Kode Etik Kedokteran (MKEK)",
            "Biro Hukum Pembinaan dan Pembelaan Anggota (BHP2A)",
            "Bidang Organisasi",
            "Bidang Pengabdian Profesi",
            "Bidang Pengembangan dan Pembinaan Profesi",
            "Bidang Kesejahteraan, Seni dan Olahraga",
            "Tim Pengembangan Pendidikan Keprofesian Berkelanjutan (P2KB)",
            "Bidang Humas"
          );
          
    }

}
