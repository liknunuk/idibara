<?php
class Model_wisata
{
    private $table = "eventwisata";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getEvents()
    {
          $this->db->query("SELECT * FROM " . $this->table ." ORDER BY id DESC LIMIT 10");
          return $this->db->resultSet();

    }

    public function newEvent($data){
        $this->db->query("INSERT INTO eventwisata
        VALUES ('',:nama,:tanggal,:lokasi,:deskripsi,:htm) ");

        // binding data, mengamankan karakter aneh
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('lokasi', $data['lokasi']);
        $this->db->bind('deskripsi', $data['deskripsi']);
        $this->db->bind('htm', $data['htm']);

        //eksekusi query setelah binding data
        $this->db->execute();

        // pesan baris terpengaruh
        return $this->db->rowCount();
    }

}
