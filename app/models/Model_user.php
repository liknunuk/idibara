<?php
class Model_user
{
    private $table = "user";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function login($password){
        $sql = "SELECT username FROM user WHERE password = :password";
        $this->db->query($sql);
        $this->db->bind('password',$password);
        $this->db->execute();
        $ndata = $this->db->rowCount();
        $adata = $this->db->resultOne();
        return array('exist'=>$ndata , 'data'=>$adata);
    }

}
