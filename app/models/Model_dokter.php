<?php
class Model_dokter
{
    private $table = "dokter";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function dftDokter($nh=1)
    {
        $row = ($nh - 1) * rows;
        $sql = "SELECT id_dokter,no_kta, nama,jk,alamat,instansi,hp FROM dokter WHERE status_idi !='mutasi' ORDER BY nama LIMIT $row,".rows; 
        
        $this->db->query($sql);
        $output = $this->db->resultSet();
        return $output;
        //echo json_encode($output,JSON_PRETTY_PRINT);
        
    }

    public function tambahDokter(){
        
        if($_POST['dok_mod'] === "baru"){

            $sql = "INSERT INTO dokter VALUES (:id,:nama,:tempat_lahir,:tgl_lahir,:jk,:alamat,:instansi,:hp,:pt1,:no_ijazah1,:tahun_lulus,:kompetensi1,:pt2,:no_ijazah2,:kompetensi2,:tahun_lulus2, :no_kta,:berlaku_kta, :no_str, :berlaku_str, :status_idi, :idi_asal, :norek_idi, :tahun_masuk, :foto , :keterangan)";
            $this->db->query($sql);

            $this->db->bind('id','');
            $this->db->bind('nama',$_POST['dok_nama']);
            $this->db->bind('tempat_lahir',$_POST['dok_tpLahir']);
            $this->db->bind('tgl_lahir',$_POST['dok_tgLahir']);
            $this->db->bind('jk',$_POST['dok_jkl']);
            $this->db->bind('alamat',$_POST['dok_alamat']);
            $this->db->bind('instansi',$_POST['dok_instansi']);
            $this->db->bind('hp',$_POST['dok_telp']);
            $this->db->bind('pt1',$_POST['dok_pend1']);
            $this->db->bind('no_ijazah1',$_POST['dok_ijazah1']);
            $this->db->bind('tahun_lulus',$_POST['dok_lulus1']);
            $this->db->bind('kompetensi1',$_POST['dok_kompet1']);
            $this->db->bind('pt2',$_POST['dok_pend2']);
            $this->db->bind('no_ijazah2',$_POST['dok_ijazah2']);
            $this->db->bind('tahun_lulus2',$_POST['dok_lulus2']);
            $this->db->bind('kompetensi2',$_POST['dok_kompet2']);
            $this->db->bind('no_kta',$_POST['dok_kta']);
            $this->db->bind('berlaku_kta',$_POST['dok_ktavalid']);
            $this->db->bind('no_str',$_POST['dok_str']);
            $this->db->bind('berlaku_str',$_POST['dok_strvalid']);
            $this->db->bind('status_idi',$_POST['dok_stidi']);
            $this->db->bind('idi_asal',$_POST['dok_idiasal']);
            $this->db->bind('norek_idi',$_POST['dok_nridi']);
            $this->db->bind('tahun_masuk',$_POST['dok_thmasuk']);
            $this->db->bind('foto','anon-doctor.jpg');
            $this->db->bind('keterangan',$_POST['dok_note']);

            $this->db->execute();

            header("Location:".BASEURL."/dokter");
            
        }elseif($_POST['dok_mod'] === "ubah"){
            
            $sql = "UPDATE dokter SET nama = :nama , tempat_lahir = :tempat_lahir , tgl_lahir = :tgl_lahir , jk = :jk,alamat = :alamat,instansi = :instansi, hp = :hp, pt1 = :pt1, no_ijazah1 = :no_ijazah1,tahun_lulus = :tahun_lulus , kompetensi1 = :kompetensi1 , pt2 = :pt2, no_ijazah2 = :no_ijazah2,kompetensi2 = :kompetensi2, tahun_lulus2 = :tahun_lulus2 , no_kta = :no_kta, berlaku_kta = :berlaku_kta , no_str = :no_str, berlaku_str = :berlaku_str , status_idi = :status_idi , idi_asal = :idi_asal ,norek_idi = :norek_idi , tahun_masuk = :tahun_masuk , keterangan = :keterangan WHERE id_dokter = :id LIMIT 1";
            $this->db->query($sql);

            $this->db->bind('id',$_POST['dok_id']);
            $this->db->bind('nama',$_POST['dok_nama']);
            $this->db->bind('tempat_lahir',$_POST['dok_tpLahir']);
            $this->db->bind('tgl_lahir',$_POST['dok_tgLahir']);
            $this->db->bind('jk',$_POST['dok_jkl']);
            $this->db->bind('alamat',$_POST['dok_alamat']);
            $this->db->bind('instansi',$_POST['dok_instansi']);
            $this->db->bind('hp',$_POST['dok_telp']);
            $this->db->bind('pt1',$_POST['dok_pend1']);
            $this->db->bind('no_ijazah1',$_POST['dok_ijazah1']);
            $this->db->bind('tahun_lulus',$_POST['dok_lulus1']);
            $this->db->bind('kompetensi1',$_POST['dok_kompet1']);
            $this->db->bind('pt2',$_POST['dok_pend2']);
            $this->db->bind('no_ijazah2',$_POST['dok_ijazah2']);
            $this->db->bind('tahun_lulus2',$_POST['dok_lulus2']);
            $this->db->bind('kompetensi2',$_POST['dok_kompet2']);
            $this->db->bind('no_kta',$_POST['dok_kta']);
            $this->db->bind('berlaku_kta',$_POST['dok_ktavalid']);
            $this->db->bind('no_str',$_POST['dok_str']);
            $this->db->bind('berlaku_str',$_POST['dok_strvalid']);
            $this->db->bind('status_idi',$_POST['dok_stidi']);
            $this->db->bind('idi_asal',$_POST['dok_idiasal']);
            $this->db->bind('norek_idi',$_POST['dok_nridi']);
            $this->db->bind('tahun_masuk',$_POST['dok_thmasuk']);
            $this->db->bind('keterangan',$_POST['dok_note']);

            $this->db->execute();
            header("Location:".BASEURL."/dokter/");
        }
    }

    public function uploadFoto($img){
        $upperdir = dirname(__DIR__, 2);
        $target_dir = $upperdir."/public/foto/";
        $target_file = $target_dir . basename($img["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $errortype = [];

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($img["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
                $msg = "Bukan berka image/citra";
                $errortype['mime']=$msg;
            }
        } 

        // Check image existent
        if (file_exists($target_file)) {
            $uploadOk = 0;
            $msg = "File sudah ada";
            $errortype['eksistensi']=$msg;
        }

        // Check file size
        if ($img["size"] > 1024000) {
            $uploadOk = 0;
            $msg = "Ukuran file melebihi batas";
            $errortype['size']=$msg;
        }
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $uploadOk = 0;
            $msg = array("Ekstensi file tidak sesuai");
            $errortype['extensi']=$msg;
        }

        if ($uploadOk == 0) {
            return array('err'=>$uploadOk, 'msg'=>$errortype);
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($img["tmp_name"], $target_file)) {
                $msg = array("Upload berhasil");
                return array('err'=>$uploadOk, 'msg'=>$msg);
            } else {
                $msg = array("Upload gagal");
                return array('err'=>$uploadOk, 'msg'=>$msg);
            }
        }
    }

    public function infoDokter($nkta){
        $sql = "SELECT * FROM dokter WHERE id_dokter = :id ";
        $this->db->query($sql);
        $this->db->bind('id',$nkta);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function faceOff(){
        
        $upload = $this->uploadFoto($_FILES['fdok_foto']);
        
       
        if($upload['err']=="1"){
            $sql = "UPDATE dokter SET foto = :foto WHERE id_dokter = :id LIMIT 1";
            $this->db->query($sql);
            $this->db->bind('foto',$_FILES['fdok_foto']['name']);
            $this->db->bind('id',$_POST['fdok_id']);
            $this->db->execute();
            //header("Location:".BASEURL."/dokter/infoDokter/{$_POST['fdok_id']}");
            echo "
            <script>
            window.location='".BASEURL."/dokter/infoDokter/{$_POST['fdok_id']}"."';
            </script>
            ";
        }
        
    }

    public function dokterByName($nama){
        $sql = "SELECT id_dokter,nama FROM dokter WHERE nama LIKE :nama LIMIT 10";
        $this->db->query($sql);
        $this->db->bind('nama',"%".$nama."%");
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function nomorKTA($nama){
        $sql = "SELECT no_kta,nama FROM dokter WHERE nama LIKE :nama LIMIT 10";
        $this->db->query($sql);
        $this->db->bind('nama',"%".$nama."%");
        $this->db->execute();
        return $this->db->resultSet();
    }

}
