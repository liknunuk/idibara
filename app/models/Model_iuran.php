<?php
class Model_iuran
{
    private $table = "iuran";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function nambahIuran(){
        $sql = "INSERT INTO iuran SET id_iuran = :id_iuran, id_dokter = :id_dokter, jenis_iuran = :jenis_iuran , mekanisme = :mekanisme , lokasi = :lokasi , iuran_dari = :iuran_dari , iuran_sampai = :iuran_sampai , nominal = :nominal , untuk_bayar = :untuk_bayar , tanggal_transaksi = :tanggal_transaksi , keterangan = :keterangan ";
        
        $this->db->query($sql);
        $this->db->bind('id_iuran',$_POST['iur_id']);
        $this->db->bind('id_dokter',$_POST['iur_dokter']);
        $this->db->bind('jenis_iuran',$_POST['iur_jenis']);
        $this->db->bind('mekanisme',$_POST['iur_mekanisme']);
        $this->db->bind('lokasi',$_POST['iur_lokasi']);
        $this->db->bind('iuran_dari',$_POST['iur_mulai']);
        $this->db->bind('iuran_sampai',$_POST['iur_sampai']);
        $this->db->bind('nominal',$_POST['iur_nominal']);
        $this->db->bind('untuk_bayar',$_POST['iur_untuk']);
        $this->db->bind('tanggal_transaksi',$_POST['iur_tanggal']);
        $this->db->bind('keterangan',$_POST['iur_note']);

        $this->db->execute();
    }

    public function ngubahIuran(){
        // echo "<pre>"; print_r($_POST); echo "</pre>";
        $sql = "UPDATE iuran 
        SET     id_dokter = :id_dokter, 
                jenis_iuran = :jenis_iuran , 
                mekanisme = :mekanisme , 
                lokasi = :lokasi , 
                iuran_dari = :iuran_dari , 
                iuran_sampai = :iuran_sampai , 
                nominal = :nominal , 
                untuk_bayar = :untuk_bayar , 
                tanggal_transaksi = :tanggal_transaksi , 
                keterangan = :keterangan 
        WHERE id_iuran = :id_iuran ";
        
        $this->db->query($sql);
        $this->db->bind('id_iuran',$_POST['iur_id']);
        $this->db->bind('id_dokter',$_POST['iur_dokter']);
        $this->db->bind('jenis_iuran',$_POST['iur_jenis']);
        $this->db->bind('mekanisme',$_POST['iur_mekanisme']);
        $this->db->bind('lokasi',$_POST['iur_lokasi']);
        $this->db->bind('iuran_dari',$_POST['iur_mulai']);
        $this->db->bind('iuran_sampai',$_POST['iur_sampai']);
        $this->db->bind('nominal',$_POST['iur_nominal']);
        $this->db->bind('untuk_bayar',$_POST['iur_untuk']);
        $this->db->bind('tanggal_transaksi',$_POST['iur_tanggal']);
        $this->db->bind('keterangan',$_POST['iur_note']);

        $this->db->execute();
    }

    public function dftIuran($th = '',$bl=''){
        if( $bl == '' || $th == '' ){
            $sql = "SELECT id_iuran , tanggal_transaksi , nama , untuk_bayar , nominal FROM iuran, dokter WHERE dokter.id_dokter = iuran.id_dokter ORDER BY tanggal_transaksi DESC LIMIT " . rows;
        }else{
            $bulan= $th."-".$bl;
            $sql = "SELECT id_iuran , tanggal_transaksi , nama , untuk_bayar , nominal FROM iuran, dokter WHERE tanggal_transaksi LIKE '{$bulan}%' && dokter.id_dokter = iuran.id_dokter ORDER BY tanggal_transaksi DESC LIMIT " . rows;
        }

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function detilIuran($id){
        $sql = "SELECT iuran.* , nama FROM iuran , dokter WHERE id_iuran = :id && dokter.id_dokter = iuran.id_dokter";
        $this->db->query($sql);
        $this->db->bind("id", $id);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function rekapIuran($thn=0){
        if( $thn == 0 ){ $thn = date('Y'); }
        $sql = "SELECT LEFT(tanggal_transaksi,7) bulan, SUM(nominal) jumlah  FROM iuran WHERE YEAR(tanggal_transaksi) = :tahun GROUP BY MONTH(tanggal_transaksi) ORDER BY tanggal_transaksi";

        $this->db->query($sql); $this->db->bind( 'tahun' , $thn );
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function iuranPerTanggal($tgl){
        $sql = "SELECT id_iuran , tanggal_transaksi , nama , untuk_bayar , nominal FROM iuran, dokter WHERE dokter.id_dokter = iuran.id_dokter && tanggal_transaksi = :tgtrx ORDER BY id_iuran DESC LIMIT " . rows;

        $this->db->query($sql);
        $this->db->bind('tgtrx' , $tgl );
        $this->db->execute();
        return $this->db->resultSet();
    }
    
    public function lunasIuran($dokId){
        $sql = "select id_dokter , max(iuran_sampai) iuranHabis from iuran where id_dokter = :id ";
        $this->db->query($sql);
        $this->db->bind('id',$dokId);
        $this->db->execute();
        $iuran = $this->db->resultOne();
        $iuranHabis = $iuran['iuranHabis'];
        if( $iuranHabis < date('Y-m-d')){
            return "Belum Lunas";
        }else{
            return "Sudah Lunas";
        }
    }

    public function tagihan(){
        $akhirBulan =  date("Y-m-t");
        $sql = "SELECT iuran.id_dokter, nama , iuran_sampai FROM iuran , dokter WHERE dokter.id_dokter = iuran.id_dokter GROUP BY id_dokter HAVING MAX(iuran_sampai) < :akhirBulan ";
        $this->db->query($sql);
        $this->db->bind('akhirBulan',$akhirBulan);
        $this->db->execute();
        return $this->db->resultSet();
    }
}