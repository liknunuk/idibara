<?php
class Model_mutasi
{
    private $table = "eventwisata";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function dataMutasi(){
        $sql = "SELECT mutasi.* , dokter.nama FROM mutasi , dokter WHERE dokter.id_dokter = mutasi.id_dokter ORDER BY tgl_mutasi DESC LIMIT 40";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function setMutasi($id){
        $sql = "UPDATE dokter SET status_idi='mutasi' WHERE id_dokter = :id ";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
    }

}
