<?php
class Model_surat
{
    private $table = "arcSurat";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function saveLokal($data){
        
        $sql = "INSERT INTO arcSurat SET id_surat = :id , tipeSurat = :tipe , nomorSurat = :nomor , tanggalSurat = :tanggal , id_dokter = :dokter , sip_ke = :sip , tempatpraktek1 = :tempat1 , alamatpraktek1 = :alamat1 , jadwalpraktek1 = :jadwal1 , tempatpraktek2 = :tempat2 , alamatpraktek2 = :alamat2 , jadwalpraktek2 = :jadwal2 , tempatpraktek3 = :tempat3 , alamatpraktek3 = :alamat3 , jadwalpraktek3 = :jadwal3 ";
        
        $this->db->query($sql);
        
        $this->db->bind('id','');
        $this->db->bind('tipe','SIP Lokal');
        $this->db->bind('nomor',$data['reg_noSurat']);
        $this->db->bind('tanggal',date('Y-m-d'));
        $this->db->bind('dokter',$data['sip_dokId']);
        $this->db->bind('sip',$data['reg_sipke']);
        $this->db->bind('tempat1',$data['reg_tempat1']);
        $this->db->bind('alamat1',$data['reg_alamat1']);
        $this->db->bind('jadwal1',$data['reg_jadwal1']);
        $this->db->bind('tempat2',$data['reg_tempat2']);
        $this->db->bind('alamat2',$data['reg_alamat2']);
        $this->db->bind('jadwal2',$data['reg_jadwal2']);
        $this->db->bind('tempat3',$data['reg_tempat3']);
        $this->db->bind('alamat3',$data['reg_alamat3']);
        $this->db->bind('jadwal3',$data['reg_jadwal3']);

        $this->db->execute();
        
    }

    public function saveTamu($data){
        $sql = "INSERT INTO arcSurat SET id_surat=:id , tipeSurat=:tipe , nomorSurat=:nomor , tanggalSurat = :tanggal , id_dokter=:dokter , nmrekomendasi=:rekom , pekerjaan=:pekerjaan , sip_ke=:sip , tempatpraktek1=:tempat1 , alamatpraktek1=:alamat1 , jadwalpraktek1=:jadwal1 , tempatpraktek2=:tempat2 , alamatpraktek2=:alamat2 , jadwalpraktek2=:jadwal2 , tempatpraktek3=:tempat3 , alamatpraktek3=:alamat3 , jadwalpraktek3=:jadwal3 ";
        $this->db->query($sql);

        $this->db->bind('id','');
        $this->db->bind('tipe','SIP Tamu');
        $this->db->bind('nomor',$data['tmu_noSurat']);
        $this->db->bind('tanggal',date('Y-m-d'));
        $this->db->bind('dokter',$data['sip_dokId']);
        $this->db->bind('rekom',$data['tmu_rekom']);
        $this->db->bind('pekerjaan',$data['tmu_kerja']);
        $this->db->bind('sip',$data['tmu_sipke']);
        $this->db->bind('tempat1',$data['tmu_tempat1']);
        $this->db->bind('alamat1',$data['tmu_alamat1']);
        $this->db->bind('jadwal1',$data['tmu_jadwal1']);
        $this->db->bind('tempat2',$data['tmu_tempat2']);
        $this->db->bind('alamat2',$data['tmu_alamat2']);
        $this->db->bind('jadwal2',$data['tmu_jadwal2']);
        $this->db->bind('tempat3',$data['tmu_tempat3']);
        $this->db->bind('alamat3',$data['tmu_alamat3']);
        $this->db->bind('jadwal3',$data['tmu_jadwal3']);

        $this->db->execute();
    }
    
    public function saveMutasi($data){
        $sql = "INSERT INTO arcSurat SET id_surat = :id , tipeSurat = :tipe , nomorSurat = :nomor , tanggalSurat = :tanggal , id_dokter = :dokter , idi_tujuan = :tujuan";

        $this->db->query($sql);

        $this->db->bind('id','');
        $this->db->bind('tipe','Mutasi');
        $this->db->bind('nomor',$data['mut_noSurat']);
        $this->db->bind('tanggal',date('Y-m-d'));
        $this->db->bind('dokter',$data['sip_dokId']);
        $this->db->bind('tujuan',$data['mut_tujuan']);

        $this->db->execute();

        $this->saveMutasiData($data);
    }

    public function saveMutasiData($data){
        $sql = "INSERT INTO mutasi SET id_dokter = :dokter , idi_tujuan = :tujuan , norek = :norek, tgl_mutasi = :tanggal ";

        $this->db->query($sql);

        $this->db->bind('dokter',$data['sip_dokId']);
        $this->db->bind('tujuan',$data['mut_tujuan']);
        $this->db->bind('norek',$data['mut_noSurat']);
        $this->db->bind('tanggal',date('Y-m-d'));

        $this->db->execute();
    }

    public function arsip(){
        $sql = "SELECT id_surat, tipeSurat, nomorSurat, DATE_FORMAT(tanggalSurat,'%d/%m/%Y') tanggalSurat, nama FROM arcSurat , dokter WHERE dokter.id_dokter = arcSurat.id_dokter ORDER BY tanggalSurat DESC LIMIT 60";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }
    
}   
