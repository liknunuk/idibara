<?php
class Model_praktik
{
    private $table = "dokter";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function dftPraktik($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT praktik.* , dokter.nama dokter FROM praktik , dokter WHERE dokter.id_dokter = praktik.id_dokter LIMIT $row ,".rows;
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function nambahPraktik(){
        // echo "<pre>"; print_r($_POST); echo "</pre>";
        $sql = "INSERT INTO praktik (id_praktik, id_dokter, sip1, nosip1, sip2, nosip2, sip3, nosip3, keterangan)  values (:id_praktik, :id_dokter, :sip1, :nosip1, :sip2, :nosip2, :sip3, :nosip3, :keterangan)";

        $this->db->query($sql);
        $this->db->bind('id_praktik',$_POST['sip_id']);
        $this->db->bind('id_dokter',$_POST['sip_dokId']);
        $this->db->bind('sip1',$_POST['sip_alamat1']);
        $this->db->bind('nosip1',$_POST['sip_nomor1']);
        $this->db->bind('sip2',$_POST['sip_alamat2']);
        $this->db->bind('nosip2',$_POST['sip_nomor2']);
        $this->db->bind('sip3',$_POST['sip_alamat3']);
        $this->db->bind('nosip3',$_POST['sip_nomor3']);
        $this->db->bind('keterangan',$_POST['sip_note']);
        $this->db->execute();
    }

    public function ngubahPraktik(){
        // echo "<pre>"; print_r($_POST); echo "</pre>";
        $sql = "UPDATE praktik SET id_dokter = :id_dokter , sip1 = :sip1 , nosip1 = :nosip1 , sip2 = :sip2 , nosip2 = :nosip2 , sip3 = :sip3 , nosip3 = :nosip3 , keterangan = :keterangan WHERE id_praktik = :id_praktik LIMIT 1";

        $this->db->query($sql);
        $this->db->bind('id_dokter',$_POST['sip_dokId']);
        $this->db->bind('sip1',$_POST['sip_alamat1']);
        $this->db->bind('nosip1',$_POST['sip_nomor1']);
        $this->db->bind('sip2',$_POST['sip_alamat2']);
        $this->db->bind('nosip2',$_POST['sip_nomor2']);
        $this->db->bind('sip3',$_POST['sip_alamat3']);
        $this->db->bind('nosip3',$_POST['sip_nomor3']);
        $this->db->bind('keterangan',$_POST['sip_note']);
        $this->db->bind('id_praktik',$_POST['sip_id']);
        $this->db->execute();
    }

    public function infoPraktik($id){ 
        $sql = "SELECT praktik.* , dokter.nama dokter FROM praktik , dokter WHERE praktik.id_praktik = :id && dokter.id_dokter = praktik.id_dokter LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function dokterPraktik($id){ 
        $sql = "SELECT praktik.* , dokter.nama dokter FROM praktik , dokter WHERE praktik.id_dokter = :dokter && dokter.id_dokter = praktik.id_dokter LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('dokter',$id);
        $this->db->execute();
        return $this->db->resultOne();
    }
}