<?php

class Controller {
  public function view($view, $data= [] )
  {
    require_once '../app/views/'. $view . '.php';
  }

  public function model( $model )
  {
    require_once '../app/models/'. $model . '.php';
    return new $model;
  }

  public function ymd2dmy($tgl){
    list($y,$m,$d) = explode("-" , $tgl);
    return "$d / $m / $y";
  }

  public function ym2my($tgl){
    list($y,$m) = explode("-" , $tgl);
    return "$m - $y";
  }

  public function nominal($rupiah){
    return number_format($rupiah,0,',','.');
  }
}
