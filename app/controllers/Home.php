<?php

class Home extends Controller
{
  // method default
  public function index()
  {
    $data['title']="IDI-BERANDA";
    $this->view('template/header',$data);
    $this->view('home/index',$data);
    $this->view('template/footer');
  }

  public function login(){
    $data['title'] = "Login IDI Banjarnegara";
    $this->view('template/cleanHeader',$data);
    $this->view('home/login');
    $this->view('template/footer');
  }

  public function logout(){
    session_unset();
    session_destroy();
    header("Location:".BASEURL."/Home/login");
  }
}
