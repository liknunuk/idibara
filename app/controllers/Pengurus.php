<?php

class Pengurus extends Controller
{
  // method default
  public function index()
  {
    $data['title']="IDI-PENGURUS";
    $data['jabatan'] = $this->model("Model_pengurus")->dftJabatan();
    $data['perakhir']= $this->model('Model_pengurus')->lastPeriod();
    $lastPer = $data['perakhir']['lper'];
    $data['lastperiod']=$lastPer;
    $data['pengurus'] = $this->model('Model_pengurus')->dftPengurus();
    $this->view('template/header',$data);
    $this->view('pengurus/index',$data);
    $this->view('template/footer');
  }

  public function save(){
    $data['title']="IDI-PENGURUS";
    $this->view('template/header',$data);
    if($_POST['idi_mod'] == "baru"){
      $this->model("Model_pengurus")->addPengurus();
    }else{
      $this->model("Model_pengurus")->chgPengurus();
    }
    $this->view('template/footer');
  }
}
