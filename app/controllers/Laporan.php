<?php

class Laporan extends Controller
{
    public function index(){
        echo "Induk Laporan";
    }
    
    public function dokter(){

        $laporan = $this->model('Model_laporan')->dokter();
        $data['judul'] = "Data Dokter";
        $data['subjek']="DOKTER";
        $data['dokter']=$laporan['data'];
        $data['jumlah']=$laporan['hasil'];
        $this->view('laporan/dokter' , $data );

    }
    
    public function iuran($bulan=""){
        if($bulan == ""){
            $bulan = date('m');
            $bulan = date('Y').'-'.sprintf('%02d',($bulan-1));
        }

        $laporan = $this->model('Model_laporan')->iuran($bulan);
        $data['judul'] = "Data Iuran";
        $data['subjek']="IURAN";
        $data['iuran']=$laporan['data'];
        $data['jumlah']=$laporan['hasil'];
        $data['bulan']=$this->ym2my($bulan);
        $this->view('laporan/iuran' , $data );
    }
    
    public function mutasi(){
        $bulan = date('m');
        $bulan = date('Y').'-'.sprintf('%02d',($bulan-1));
        $laporan = $this->model('Model_laporan')->mutasi($bulan);
        $data['judul'] = "Data Mutasi";
        $data['subjek']="MUTASI";
        $data['mutasi']=$laporan['data'];
        $data['jumlah']=$laporan['hasil'];
        $data['bulan']=$this->ym2my($bulan);
        $this->view('laporan/mutasi' , $data );
    }
    
    public function pengurus(){
        
        $data['title']="IDI-PENGURUS";
        $data['jabatan'] = $this->model("Model_pengurus")->dftJabatan();
        $data['perakhir']= $this->model('Model_pengurus')->lastPeriod();
        $lastPer = $data['perakhir']['lper'];
        $data['lastperiod']=$lastPer;
        $data['pengurus'] = $this->model('Model_pengurus')->dftPengurus();
        $this->view('laporan/pengurus',$data);
    }
    
    public function praktik(){
        $data['title'] = 'LAPORAN PRAKTIK';
        $data['subjek']="PRAKTIK";
        $data['praktik']=$this->model('Model_praktik')->dftPraktik();
        $this->view('laporan/praktik',$data);
    }
    
    public function surat(){
        $data['title']="IDI-SIP";
        $data['subjek']="SURAT";
        $data['arsip']=$this->model("Model_surat")->arsip();
        $this->view('laporan/surat',$data);
    }

}