<?php

class Surat extends Controller
{
  // method default
  public function index()
  {
    $data['title']="IDI-SIP";
    $data['arsip']=$this->model("Model_surat")->arsip();
    $this->view('template/header',$data);
    $this->view('sip/index',$data);
    $this->view('template/footer');
  }

  public function siplokal()
  {
    $data['title']="IDI-SIP";
    $this->view('template/header',$data);
    $this->view('sip/siplokal',$data);
    $this->view('template/footer');
  }

  public function siptamu()
  {
    $data['title']="IDI-SIP";
    $this->view('template/header',$data);
    $this->view('sip/siptamu',$data);
    $this->view('template/footer');
  }

  public function mutasi()
  {
    $data['title']="IDI-SIP";
    $this->view('template/header',$data);
    $this->view('sip/mutasi',$data);
    $this->view('template/footer');
  }

  public function save($tipe)
  {
    $data['title']="IDI-SIP";
    $data['sip']=$_POST;
    $data['ketua']=$this->model("Model_pengurus")->ketuanya();
    $data['dokter']=$this->model("Model_dokter")->infoDokter($_POST['sip_dokId']);
    // echo "<pre>"; print_r($_POST); echo "</pre>";
    // $this->view('template/header',$data);
    if( $tipe == "reguler"){
      
      $this->view('sip/suratLokal' , $data);
      $this->model("Model_surat")->saveLokal($_POST);
      
    }elseif( $tipe == "tamu"){

      $this->view('sip/suratTamu' , $data);
      $this->model("Model_surat")->saveTamu($_POST);

    }elseif( $tipe == "mutasi"){

      $this->view('sip/suratMutasi' , $data);
      $this->model("Model_surat")->saveMutasi($_POST);
      $this->model('Model_mutasi')->setMutasi($_POST['sip_dokId']);
      
    }else{
      header("Location:".BASEURL."/surat/arsip");
    }
    $this->view('template/footer');
  }

  
}
