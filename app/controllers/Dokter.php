<?php

class Dokter extends Controller
{
  // method default
  public function index()
  {
    $data['title']="IDI-DOKTER";
    $data['dokter']=$this->model('Model_dokter')->dftDokter();
    $this->view('template/header',$data);
    $this->view('dokter/index',$data);
    $this->view('template/footer');
  }

  public function save(){
    $data['title']="IDI-DOKTER";
    $this->view('template/header',$data);
    $dok = $this->model('Model_dokter')->tambahDokter();
    $this->view('template/footer');
    // header("Location:" . BASEURL ."/dokter");
    echo "
    <script>window.location='". BASEURL ."/dokter';</script>
    ";
  }

  public function infoDokter($id){
    $data['title']="IDI-DOKTER";
    $data['dokter']=$this->model('Model_dokter')->infoDokter($id);
    $data['praktik']=$this->model('Model_praktik')->dokterPraktik($id);
    $this->view('template/header',$data);
    $this->view('dokter/info',$data);
    $this->view('template/footer');
  }

  public function faceOff(){
    $data['title']="IDI-DOKTER";
    $this->view('template/header',$data);
    $this->model('Model_dokter')->faceOff();
    $this->view('template/footer');
  }
}
