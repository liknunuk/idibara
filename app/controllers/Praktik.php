<?php

class Praktik extends Controller
{
  // method default
  public function index()
  {
    $data['title']="IDI-PRAKTIK";
    $data['praktik'] = $this->model('Model_praktik')->dftPraktik(1);
    $this->view('template/header',$data);
    $this->view('praktik/index',$data);
    $this->view('template/footer');
  }

  public function save(){
    $data['title']="IDI-PRAKTIK";
    $this->view('template/header',$data);
    // echo "<pre>";print_r($_POST);echo "</pre>";
    if($_POST['sip_mod'] == 'baru' ){
      $dok = $this->model('Model_praktik')->nambahPraktik();
    }else{
      $dok = $this->model('Model_praktik')->ngubahPraktik();
    }
    $this->view('template/footer');
    header("Location:" . BASEURL ."/praktik");
  }
}
