<?php

class Mutasi extends Controller
{
  // method default
  public function index()
  {
    $data['title']="IDI-MUTASI";
    $data['mutasi'] = $this->model('Model_mutasi')->dataMutasi();
    $dokId = $data['mutasi']['id_dokter'];
    $data['dokter'] = $this->model('Model_dokter')->infoDokter($dokId);
    $this->view('template/header',$data);
    $this->view('mutasi/index',$data);
    $this->view('template/footer');
  }
}
