<?php

class Pusdata extends Controller
{
  // method default
  public function index()
  {
    echo "Layanan Data Ikatan Dokter Indonesia";
  }

  public function dftDokter($pn=1){
    $dokter = $this->model('Model_dokter')->dftDokter($pn);
  }

  public function infoDokter($nkta){
    $dokter = $this->model('Model_dokter')->infoDokter($nkta);
    echo json_encode($dokter , JSON_PRETTY_PRINT);
  }

  public function dokterByName($nama){
    $dokter = $this->model('Model_dokter')->dokterByName($nama);
    echo json_encode($dokter,JSON_PRETTY_PRINT);
  }

  public function npaidi($nama){
    $dokter = $this->model('Model_dokter')->nomorKTA($nama);
    echo json_encode($dokter,JSON_PRETTY_PRINT);
  }

  public function infoPraktik($id){
    $praktik = $this->model('Model_praktik')->infoPraktik($id);
    echo json_encode($praktik, JSON_PRETTY_PRINT);
  }

  public function dokterPraktik($id){
    $praktik = $this->model('Model_praktik')->infoPraktik($id);
    echo json_encode($praktik, JSON_PRETTY_PRINT);
  }

  public function dftPengurus($p1,$p2){
    $pengurus = $this->model("Model_pengurus")->dftPengurus($p1,$p2);
    echo json_encode($pengurus, JSON_PRETTY_PRINT);
  }

  public function delpeng(){
    $this->model("Model_pengurus")->rmvPengurus($_POST['idx']);
  }

  public function infoIuran($idx){
    $iuran = $this->model("Model_iuran")->detilIuran($idx);
    echo json_encode($iuran, JSON_PRETTY_PRINT);
  }

  public function iurByDate($tgl){
    $iuran = $this->model("Model_iuran")->iuranPerTanggal($tgl);
    echo json_encode($iuran, JSON_PRETTY_PRINT);
  }

  public function jabatanAnggota($id){
    $jabatan = $this->model("Model_pengurus")->dftJabatan();
    // print_r($jabatan);
    // echo "<hr>";
    $penguruskah = $this->model("Model_pengurus")->penguruskah($id);
    //print_r($penguruskah);
    if( $penguruskah['qty'] != 0 ){
      $idxJabatan = $penguruskah['jabatan'];
      $kedudukan = array('jabatan'=>$jabatan[$idxJabatan]);
    }else{
      $kedudukan = array('jabatan'=>"Anggota Biasa");
    }

    echo json_encode($kedudukan);

  }

  public function cekIuran($id){
    $iuran = $this->model('Model_iuran')->lunasIuran($id);
    echo $iuran;
  }

  public function uauth(){
    $u = $_POST['username'];
    $p = $_POST['password'];

    $password = md5("*{$u}#{$p}2019");
    $user = $this->model('Model_user')->login($password);
    /* Array([exist] => 1 , [data] => Array([username] => admin ))*/
    if($user['exist'] >= 1){
      session_start();
      $_SESSION['user'] = $user['data']['username'];
      header("Location:".BASEURL);
    }
  }

}
