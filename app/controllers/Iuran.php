<?php

class Iuran extends Controller
{
  // method default
  public function index($th='',$bl='')
  {
    $data['title']="IDI-IURAN";
    $data['iuran'] = $this->model("Model_iuran")->dftIuran($th,$bl);
    $this->view('template/header',$data);
    $this->view('iuran/index',$data);
    $this->view('template/footer');
  }

  public function rekap(){
    $data['title']="IDI-IURAN";
    $data['rekap'] = $this->model("Model_iuran")->rekapIuran();
    $this->view('template/header',$data);
    $this->view('iuran/rekap',$data);
    $this->view('template/footer');
  }

  public function save(){
    $data['title']="IDI-IURAN";
    $this->view('template/header',$data);
    if($_POST['iur_mod'] == 'baru'){
      $this->model("Model_iuran")->nambahIuran();
    }else{
      $this->model("Model_iuran")->ngubahIuran();
    }
    $this->view('template/footer');
    header("Location:". BASEURL . "/iuran");
  }

  public function receipt($id){
    $data['iuran']=$this->model('Model_iuran')->detilIuran($id);
    $data['bandar']=$this->model('Model_pengurus')->bendaharanya();
    $this->view('iuran/receipt',$data);
  }

  public function tagihan(){
    $data['tagihan'] = $this->model('Model_iuran')->tagihan();
    $data['title']="IDI-IURAN";
    $this->view('template/header',$data);
    $this->view('iuran/tagihan',$data);
    $this->view('template/footer');
    
  }
}
