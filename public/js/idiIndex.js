$("#idi_dokter").keyup( function(){
    let dok = $(this).val();
    if( dok.length >=3 ){

        $.getJSON( pusdata + `/dokterByName/${dok}`,function(dokter){
            $("#dokList option").remove();
            $.each( dokter , function(i,data) {
                $("#dokList").append(
                    `<option value='${data.id_dokter}'>${data.nama}</option>`
                );  
            })
        })
    }
})

$(".fa-trash-o").click( function(){
    let idx = $(this).prop('id');
    // alert(idx);
    var tenan = confirm("Data akan dihapus!!");
    if( tenan == true ){
        $.post( pusdata + `/delpeng` , { idx : idx } , function(resp){ location.reload(); });
    }
});