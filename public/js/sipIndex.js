$("#sip_add").click( function(){
    $("#sip_id").val('');
    $("#sip_mod").val('baru');
})

$("#sip_dokId").keyup( function(){
    let dokname= $(this).val();
    if(dokname.length >= 3){
        $.getJSON(pusdata+`/dokterByName/${dokname}` , function(dokters){
            $("#sip_dokList option").remove();
            $.each(dokters , function(i,data){
                $("#sip_dokList").append(`<option value='${data.id_dokter}'>${data.nama}</option>`);
            })

        })
    }
})

$("#sip_dokId").change( function(){
    let dokId = $(this).val();
    let pesan ='';
    $.ajax({
        url: pusdata + `/cekIuran/${dokId}` ,
        success : function(iuran){
            if(iuran == 'Sudah Lunas'){
                pesan = "<div class='bg-primary text-center'>"+iuran+" Iuran</div>";
            }else{
                pesan = "<div class='bg-danger text-light text-center'>"+iuran+" Iuran</div>";
                $('input').prop('disabled',true);
                $('button').hide();
            }
            $("#infoIuran").html(pesan);
        }
    })
})

$('.sip-edit').click( function(){
    var sipId = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    // alert(sipId);
    $("#modalPraktik").modal('show');

    $.getJSON(pusdata + `/infoPraktik/${sipId}` , function(sips){
        // console.log(sips);
        $('input').val('');
        $('#sip_id').val(sips.id_praktik);
        $('#sip_mod').val('ubah');
        $('#sip_dokId').val(sips.id_dokter);
        $('#sip_alamat1').val(sips.sip1);
        $('#sip_nomor1').val(sips.nosip1);
        $('#sip_alamat2').val(sips.sip2);
        $('#sip_nomor2').val(sips.nosip2);
        $('#sip_alamat3').val(sips.sip3);
        $('#sip_nomor3').val(sips.nosip3);
        $('#sip_note').val(sips.keterangan);
    })
})

$("#sip_dokId").on('change',function(){
    let dokId = $('#sip_dokId').val();
    $.getJSON( pusdata + `/jabatanAnggota/${dokId}` , function(jbt){
        // console.log(jbt.jabatan);
        $("#mut_jabat").val(jbt.jabatan);
        $("#mut_tujuan").focus();
    })
})