
$('.dok-info').click( function(){
    let dokId = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    // alert('info dokter ' + dokId);
    window.location = baseurl + "/dokter/infoDokter/"+dokId;
})

$('.dok-edit').click( function(){
    let dokId = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    $.getJSON(pusdata+`/infoDokter/${dokId}`,function(dokter){
        $('input').val('');
        $("#dok_mod").val('ubah');
        $("#dok_id").val(dokId);
        $("#dok_nama").val(dokter.nama);
        $("#dok_tpLahir").val(dokter.tempat_lahir);
        $("#dok_tgLahir").val(dokter.tgl_lahir);
        $("#dok_alamat").val(dokter.alamat);
        $("#dok_telp").val(dokter.hp);
        $("#dok_instansi").val(dokter.instansi);
        $("#dok_pend1").val(dokter.pt1);
        $("#dok_ijazah1").val(dokter.no_ijazah1);
        $("#dok_lulus1").val(dokter.tahun_lulus);
        $("#dok_kompet1").val(dokter.kompetensi1);
        $("#dok_pend2").val(dokter.pt2);
        $("#dok_ijazah2").val(dokter.no_ijazah2);
        $("#dok_lulus2").val(dokter.tahun_lulus2);
        $("#dok_kompet2").val(dokter.kompetensi2);
        $("#dok_kta").val(dokter.no_kta);
        $("#dok_ktavalid").val(dokter.berlaku_kta);
        $("#dok_str").val(dokter.no_str);
        $("#dok_strvalid").val(dokter.berlaku_str);
        $("#dok_stidi").val(dokter.status_idi);
        $("#dok_idiasal").val(dokter.idi_asal);
        $("#dok_nridi").val(dokter.norek_idi);
        $("#dok_thmasuk").val(dokter.tahun_masuk);
        $("#dok_note").val(dokter.keterangan);
    });

    $('#modalDokter').modal('show');
})

$('.dok-kick').click( function(){
    let dokId = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    alert('remove dokter ' + dokId);
})
$('#gantiFoto').click( function(){
    let dokId = $("#lblDokId").text();
    $("#modalFDokter").modal('show');
    $("#fdok_id").val(dokId);
})