$(document).ready( function(){

    $('#iur_dokter').keyup(function(){
        let dokName = $(this).val();
        if( dokName.length >=3 ){
            $.getJSON( pusdata +`/dokterByName/${dokName}` , function(dokters){
                $("#dokList option").remove();
                $.each(dokters , function(i,data){
                    $("#dokList").append(`<option value='${data.id_dokter}'>${data.nama}</option>`);
                })
            })
        }
    })
    
    $(".iur-info").click(function(){
        let idx = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        $.getJSON( pusdata + `/infoIuran/${idx}` , function(iuran){
            $("#modalIurinfo").modal("show");
            $("#dinf_idx").text(iuran.id_iuran);
            $("#dinf_dok").text(iuran.nama);
            let tgIuran = ymd2dmy(iuran.tanggal_transaksi);
            $("#dinf_tgl").text(tgIuran);
            $("#dinf_nominal").text(parseInt(iuran.nominal).toLocaleString("id-ID"));
            $("#dinf_untuk").text(iuran.untuk_bayar);
            let iurdari,iursampai,iurperiod;
            iurdari = ymd2dmy(iuran.iuran_dari);
            iursampai = ymd2dmy(iuran.iuran_sampai);
            iurperiod = iurdari + " s.d " + iursampai;
            $("#dinf_periode").text(iurperiod);
            $("#dinf_jenis").text(iuran.jenis_iuran);
            $("#dinf_mekanisme").text(iuran.mekanisme);
            $("#dinf_lokasi").text(iuran.lokasi);
            $("#dinf_keterangan").text(iuran.keterangan);
        })
        
    })
    
    $(".iur-edit").click(function(){
        let idx = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        $.getJSON( pusdata + `/infoIuran/${idx}` , function(iuran){
            $('#modalIuran').modal('show');
            $("#iur_mod").val("ubah");
            $("#iur_id").val(iuran.id_iuran);
            $("#iur_dokter").val(iuran.id_dokter);
            $("#iur_jenis").val(iuran.jenis_iuran);
            $("#iur_mekanisme").val(iuran.mekanisme);
            $("#iur_lokasi").val(iuran.lokasi);
            $("#iur_mulai").val(iuran.iuran_dari);
            $("#iur_sampai").val(iuran.iuran_sampai);
            $("#iur_nominal").val(iuran.nominal);
            $("#iur_untuk").val(iuran.untuk_bayar);
            $("#iur_tanggal").val(iuran.tanggal_transaksi);
            $("#iur_note").val(iuran.keterangan);
        })
    
    })

    $("#iur_fsrc").on( "submit" , function(e){
        e.preventDefault();
        let fdate = $("#iur_fdate").val();
        $.getJSON( pusdata + `/iurByDate/${fdate}` , function(iuran){
            $("#iur_data tr").remove();
            $.each(iuran , function(i,data){
                let tgTrx = ymd2dmy(data.tanggal_transaksi);
                let rpiah = parseInt(data.nominal);
                let nominal = rpiah.toLocaleString("id-ID");
                $("#iur_data").append(`
                <tr>
                    <td>${data.id_iuran}</td>
                    <td>${tgTrx}</td>
                    <td>${data.nama}</td>
                    <td align="right">${nominal}&nbsp;</td>
                    <td>${data.untuk_bayar}</td>
                    <td>
                        <i class="fa fa-credit-card btn btn-primary iur-info"></i>
                        <i class="fa fa-edit btn btn-warning iur-edit"></i>
                        <i class="fa fa-trash-o btn btn-danger iur-rmve"></i>
                    </td>
                </tr>
                `);
            })            
        })
    })

    $(".iur-rcpt").click( function(){
        let idIuran = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        window.location=baseurl+`/iuran/receipt/${idIuran}`;
    })
})


function ymd2dmy(tanggal){
    var tgl = tanggal.split("-");
    return tgl[2]+"/"+tgl[1]+"/"+tgl[0];
}